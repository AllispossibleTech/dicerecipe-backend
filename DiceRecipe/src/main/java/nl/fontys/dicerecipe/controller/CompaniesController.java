package nl.fontys.dicerecipe.controller;


import lombok.AllArgsConstructor;
import nl.fontys.dicerecipe.business.CompaniesService;
import nl.fontys.dicerecipe.domain.accounts.Company;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.security.RolesAllowed;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/companies")
@AllArgsConstructor
@CrossOrigin(origins="http://localhost:3000")
public class CompaniesController {
    private final CompaniesService companiesService;

    @GetMapping
    public ResponseEntity<List<Company>> getCompanies() {
        return ResponseEntity.ok(companiesService.getCompanies());
    }

    //@IsAuthenticated
    //@RolesAllowed({"COMPANY"})
    @GetMapping("{id}")
    public ResponseEntity<Company> getCompany(@PathVariable(value = "id") final int id) {
        final Optional<Company> companyOptional = companiesService.getCompany(id);
        if (companyOptional.isEmpty()) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok().body(companyOptional.get());
    }

    @PostMapping()
    public ResponseEntity<String> createCompany(@RequestBody Company company) {
        String response = companiesService.createCompany(company);
        return ResponseEntity.status(HttpStatus.CREATED).body(response);
    }

}
