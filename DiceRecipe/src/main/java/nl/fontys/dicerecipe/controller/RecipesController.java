package nl.fontys.dicerecipe.controller;

import lombok.AllArgsConstructor;
import nl.fontys.dicerecipe.business.RecipesService;
import nl.fontys.dicerecipe.configuration.security.isauthenticated.IsAuthenticated;
import nl.fontys.dicerecipe.domain.Product;
import nl.fontys.dicerecipe.domain.Recipe;
import nl.fontys.dicerecipe.domain.requests.SearchRecipesByRangeRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.security.RolesAllowed;
import java.util.List;

@RestController
@RequestMapping("/recipes")
@AllArgsConstructor
@CrossOrigin(origins="http://localhost:3000")
public class RecipesController {
    private final RecipesService recipesService;
    @GetMapping
    public ResponseEntity<List<Recipe>> getRecipes() {
        return ResponseEntity.ok(recipesService.getRecipes());
    }

    @IsAuthenticated
    @RolesAllowed({"COMPANY"})
    @PostMapping()
    public ResponseEntity<String> createRecipe(@RequestBody Recipe recipe) {
        String response = recipesService.addRecipe(recipe);
        return ResponseEntity.status(HttpStatus.CREATED).body(response);
    }

    @IsAuthenticated
    @RolesAllowed({"CONSUMER"})
    @GetMapping("/loadsuggestions/{id}")
    public ResponseEntity<List<Recipe>> getSuggestedRecipes(@PathVariable(value = "id") final int id) {
        return ResponseEntity.ok(recipesService.loadSuggestions(id));
    }

    @IsAuthenticated
    @RolesAllowed({"COMPANY"})
    @GetMapping("/company/{id}")
    public ResponseEntity<List<Recipe>> getRecipesByCompanyId(@PathVariable(value = "id") final int id) {
        return ResponseEntity.ok(recipesService.getRecipesByCompanyDetailsId(id));
    }

    @IsAuthenticated
    @RolesAllowed({"COMPANY"})
    @PostMapping("/delete")
    public ResponseEntity<String> deleteProduct(@RequestBody Recipe recipe) {
        recipesService.delete(recipe);
        return ResponseEntity.ok("deleted");
    }

    @GetMapping("{id}")
    public ResponseEntity<Recipe> getRecipeById(@PathVariable(value = "id") final int id) {
        return ResponseEntity.ok(recipesService.getRecipeById(id));
    }

    @RolesAllowed({"CONSUMER"})
    @GetMapping("/time-to-prepare/{id}")
    public ResponseEntity<List<Recipe>> getRecipesByTimeToPrepare(@PathVariable(value = "id") final int id) {
        return ResponseEntity.ok(recipesService.getRecipesByTimeToPrepare(id));
    }

    @GetMapping("/time-to-prepare-range")
    public ResponseEntity<List<Recipe>> getRecipesByTimeToPrepare(
            @RequestParam("min") int min,
            @RequestParam("max") int max) {
        SearchRecipesByRangeRequest request = new SearchRecipesByRangeRequest(min, max);
        return ResponseEntity.ok(recipesService.getRecipesByRangeOfTimeToPrepare(request));
    }
}