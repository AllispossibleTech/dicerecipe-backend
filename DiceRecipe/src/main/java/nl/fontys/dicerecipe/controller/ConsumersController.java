package nl.fontys.dicerecipe.controller;

import lombok.AllArgsConstructor;
import nl.fontys.dicerecipe.business.ConsumersService;
import nl.fontys.dicerecipe.configuration.security.isauthenticated.IsAuthenticated;
import nl.fontys.dicerecipe.domain.accounts.Consumer;
import nl.fontys.dicerecipe.domain.requests.AddRemoveProductToInventoryRequest;
import nl.fontys.dicerecipe.domain.requests.UpdateTimeRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.security.RolesAllowed;
import java.util.Optional;

@RestController
@RequestMapping("/consumers")
@AllArgsConstructor
@CrossOrigin(origins="http://localhost:3000")
public class ConsumersController {

    private final ConsumersService consumersService;

    @GetMapping("{id}")
    @IsAuthenticated
    @RolesAllowed({"CONSUMER"})
    public ResponseEntity<Consumer> getConsumer(@PathVariable(value = "id") final int id) {
        final Optional<Consumer> consumerOptional = consumersService.getConsumer(id);
        if (consumerOptional.isEmpty()) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok().body(consumerOptional.get());
    }

    @PostMapping()
    public ResponseEntity<String> createConsumer(@RequestBody Consumer consumer) {
        String response = consumersService.createConsumer(consumer);
        return ResponseEntity.status(HttpStatus.CREATED).body(response);
    }

    @IsAuthenticated
    @RolesAllowed({"CONSUMER"})
    @PostMapping("/addproduct")
    public ResponseEntity<String> addProductToInventory(@RequestBody AddRemoveProductToInventoryRequest request) {
        String response = consumersService.addProductToInventory(request);
        return ResponseEntity.status(HttpStatus.CREATED).body(response);
    }

    @IsAuthenticated
    @RolesAllowed({"CONSUMER"})
    @PostMapping("/removeproduct")
    public ResponseEntity<String> removeProductToInventory(@RequestBody AddRemoveProductToInventoryRequest request) {
        String response = consumersService.removeProductFromInventory(request);
        return ResponseEntity.status(HttpStatus.CREATED).body(response);
    }

    @IsAuthenticated
    @RolesAllowed({"CONSUMER"})
    @PostMapping("/update-time")
    public ResponseEntity<String> updatePreferredCookingTime(@RequestBody UpdateTimeRequest request) {
        String response = consumersService.updatePreferredCookingTime(request);
        return ResponseEntity.status(HttpStatus.CREATED).body(response);
    }
}
