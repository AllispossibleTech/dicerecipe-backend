package nl.fontys.dicerecipe.domain.accounts;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Account {

    private int id;
    private String name;
    private String email;
    private String password;
    private UserType userType;

    public Account(String name, String email, String password, UserType userType)
    {
        this.name = name;
        this.email = email;
        this.password = password;
        this.userType = userType;
    }
}