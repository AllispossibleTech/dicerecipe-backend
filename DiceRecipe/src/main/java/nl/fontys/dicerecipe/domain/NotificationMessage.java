package nl.fontys.dicerecipe.domain;

import lombok.*;

@EqualsAndHashCode
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class NotificationMessage {
    private String id;
    private String from;
    private String to;
    private String text;
}