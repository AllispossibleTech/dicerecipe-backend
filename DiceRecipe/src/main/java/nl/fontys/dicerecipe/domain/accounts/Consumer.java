package nl.fontys.dicerecipe.domain.accounts;

import lombok.*;
import nl.fontys.dicerecipe.domain.Product;
import nl.fontys.dicerecipe.domain.Recipe;

import java.util.ArrayList;
import java.util.List;

@EqualsAndHashCode
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class Consumer {

    private int id;
    private int preferredCookingTime;
    private List<Product> foodInventory;
    private List<Recipe> doneRecipes;
    private Account account;

    public Consumer(int id, Account account, int preferredCookingTime, List<Product> foodInventory, List<Recipe> doneRecipes) {

        this.id = id;
        this.account = account;
        this.preferredCookingTime = preferredCookingTime;
        this.foodInventory = foodInventory;
        this.doneRecipes = doneRecipes;

    }

    public Consumer(Account account, int preferredCookingTime) {

        this.account = account;
        this.preferredCookingTime = preferredCookingTime;
        this.foodInventory = new ArrayList<Product>();
        this.doneRecipes = new ArrayList<Recipe>();
    }

}