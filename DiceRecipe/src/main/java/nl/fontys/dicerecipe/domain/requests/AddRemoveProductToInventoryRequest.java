package nl.fontys.dicerecipe.domain.requests;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AddRemoveProductToInventoryRequest {
    int productId;
    int consumerId;
}
