package nl.fontys.dicerecipe.domain.accounts;

public enum UserType {
    COMPANY,
    CONSUMER
}
