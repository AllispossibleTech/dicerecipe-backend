package nl.fontys.dicerecipe.domain.login;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AccessToken {
    private String subject;
    private String role;
    private Integer userId;
    private Integer roleId;

    @JsonIgnore
    public boolean hasRole(String roleName) {
        if (role == null) {
            return false;
        }
        return role.contains(roleName);
    }
}
