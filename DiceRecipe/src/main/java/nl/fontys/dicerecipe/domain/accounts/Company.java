package nl.fontys.dicerecipe.domain.accounts;

import lombok.*;
import nl.fontys.dicerecipe.domain.CompanyDetails;

@EqualsAndHashCode
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class Company {

    private int id;
    private CompanyDetails details;
    private Account account;

    public Company(int id, Account account, CompanyDetails companyDetails) {

        this.id = id;
        this.details = companyDetails;
        this.account = account;
    }

    public Company(Account account, CompanyDetails companyDetails) {

        this.details = companyDetails;
        this.account = account;
    }
}