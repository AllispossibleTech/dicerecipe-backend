package nl.fontys.dicerecipe.persistence.DataAccessImpl.SpringImpl;

import nl.fontys.dicerecipe.persistence.Entities.AccountEntity;
import nl.fontys.dicerecipe.persistence.Entities.RecipeEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface AccountJPA extends JpaRepository<AccountEntity, Integer>  {

    Optional<AccountEntity> findById(Integer id);

    Optional<AccountEntity> findByEmail(String email);

    List<AccountEntity> findAll();

    AccountEntity save(AccountEntity accountEntity);

    void delete(AccountEntity accountEntity);
}
