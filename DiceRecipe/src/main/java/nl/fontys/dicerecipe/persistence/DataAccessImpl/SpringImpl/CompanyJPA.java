package nl.fontys.dicerecipe.persistence.DataAccessImpl.SpringImpl;

import nl.fontys.dicerecipe.domain.accounts.Company;
import nl.fontys.dicerecipe.persistence.Entities.CompanyEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface CompanyJPA extends JpaRepository<CompanyEntity, Integer> {

    Optional<CompanyEntity> findById(Integer id);
    CompanyEntity findByAccount_Id(Integer id);

    List<CompanyEntity> findAll();

    CompanyEntity save(CompanyEntity company);

    void delete(CompanyEntity company);
}
