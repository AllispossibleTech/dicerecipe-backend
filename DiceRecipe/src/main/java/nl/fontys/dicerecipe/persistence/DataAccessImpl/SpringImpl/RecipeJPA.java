package nl.fontys.dicerecipe.persistence.DataAccessImpl.SpringImpl;

import nl.fontys.dicerecipe.persistence.Entities.ProductEntity;
import nl.fontys.dicerecipe.persistence.Entities.RecipeEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface RecipeJPA extends JpaRepository<RecipeEntity, Integer> {

    Optional<RecipeEntity> findById(Integer id);

    List<RecipeEntity> findAll();

    RecipeEntity save(RecipeEntity recipeEntity);

    void delete(RecipeEntity recipeEntity);

    @Query("SELECT r FROM RecipeEntity r WHERE r.timeToPrepare BETWEEN :minTime AND :maxTime")
    List<RecipeEntity> findByTimeToPrepareRange(int minTime, int maxTime);


}
