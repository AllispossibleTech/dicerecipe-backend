package nl.fontys.dicerecipe.persistence.Entities;


import javax.persistence.*;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "companydetails")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CompanyDetailsEntity {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "name")
    private String name;

    //@NotBlank
    //@Length(min = 2, max = 50)
    @Column(name = "address")
    private String address;

    //@NotBlank
    //@Length(min = 2, max = 50)
    @Column(name = "vatnumber")
    private String vatNumber;
}
