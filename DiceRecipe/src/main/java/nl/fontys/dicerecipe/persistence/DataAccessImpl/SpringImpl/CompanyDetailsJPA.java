package nl.fontys.dicerecipe.persistence.DataAccessImpl.SpringImpl;

import nl.fontys.dicerecipe.persistence.Entities.AccountEntity;
import nl.fontys.dicerecipe.persistence.Entities.CompanyDetailsEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface CompanyDetailsJPA extends JpaRepository<CompanyDetailsEntity, Integer> {

    Optional<CompanyDetailsEntity> findById(Integer id);

    List<CompanyDetailsEntity> findAll();

    CompanyDetailsEntity save(CompanyDetailsEntity companyDetailsEntity);

    void delete(CompanyDetailsEntity companyDetailsEntity);
}