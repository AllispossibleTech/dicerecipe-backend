package nl.fontys.dicerecipe.persistence.Entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Set;

@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "consumers")
public class ConsumerEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "preferredcookingtime")
    private int preferredCookingTime;

    @OneToOne
    @JoinColumn(name = "account_id")
    private AccountEntity accountEntity;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "consumer_products",
            joinColumns = @JoinColumn(name = "consumer_id"),
            inverseJoinColumns = @JoinColumn(name = "product_id")
    )
    private Set<ProductEntity> foodInventoryEntities;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "consumer_recipes",
            joinColumns = @JoinColumn(name = "consumer_id"),
            inverseJoinColumns = @JoinColumn(name = "recipe_id")
    )
    private Set<RecipeEntity> doneRecipeEntities;
}