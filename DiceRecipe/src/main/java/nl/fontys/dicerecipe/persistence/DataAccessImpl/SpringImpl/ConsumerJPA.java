package nl.fontys.dicerecipe.persistence.DataAccessImpl.SpringImpl;

import nl.fontys.dicerecipe.domain.accounts.Consumer;
import nl.fontys.dicerecipe.persistence.Entities.ConsumerEntity;
import nl.fontys.dicerecipe.persistence.Entities.ProductEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.Set;

@Repository
public interface ConsumerJPA extends JpaRepository<ConsumerEntity, Integer>  {

    /* CUSTOM QUERIES ->

    @Query("select ce from ConsumerEntity ce where ce.id=:accountId")
    ConsumerEntity getConsumerByAccountId_CUSTOM(@Param(value = "accountId") Integer accountId);

    @Modifying
    @Query(
            value = "INSERT INTO dicerecipe.consumer_products (consumer_id, product_id) VALUES(?1, ?2)",
            nativeQuery = true)
    void addProductToInventory(int consumerId, int productId);


    @Transactional
    @Modifying
    @Query("UPDATE ConsumerEntity c SET c.foodInventoryEntities = :productEntities WHERE c.id = :consumerId")
    void updateFoodInventory(@Param("consumerId") Integer consumerId, @Param("productEntities") Set<ProductEntity> productEntities);
    */


    Optional<ConsumerEntity> findById(Integer id);
    ConsumerEntity findByAccountEntity_Id(Integer id);
    List<ConsumerEntity> findAll();
    ConsumerEntity save(ConsumerEntity consumer);
    void delete(ConsumerEntity consumer);
}
