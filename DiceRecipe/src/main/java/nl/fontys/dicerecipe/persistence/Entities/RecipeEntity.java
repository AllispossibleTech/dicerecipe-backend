package nl.fontys.dicerecipe.persistence.Entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;
import java.util.Set;

@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "recipes")
public class RecipeEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "name")
    private String title;

    @Column(name = "description")
    private String description;
    @Column(name = "timetoprepare")
    private int timeToPrepare;

    @ManyToOne
    @JoinColumn(name = "companydetails_id")
    private CompanyDetailsEntity companyDetailsEntity;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "recipe_products",
            joinColumns = @JoinColumn(name = "recipe_id"),
            inverseJoinColumns = @JoinColumn(name = "product_id")
    )
    private Set<ProductEntity> ingredients;

    @ManyToMany(mappedBy = "doneRecipeEntities")
    List<ConsumerEntity> doneBy;
}
