package nl.fontys.dicerecipe.persistence.DataAccessImpl.SpringImpl;

import nl.fontys.dicerecipe.persistence.Entities.ProductEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ProductJPA extends JpaRepository<ProductEntity, Integer> {

    Optional<ProductEntity> findById(Integer id);

    List<ProductEntity> findAll();

    ProductEntity save(ProductEntity productEntity);

    void delete(ProductEntity productEntity);


    // @Query("select")
    //Optional<ProductEntity> nonCrud();

}
