package nl.fontys.dicerecipe.persistence.Entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.persistence.*;

@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "companies")
public class CompanyEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @OneToOne
    @JoinColumn(name = "account_id")
    private AccountEntity account;

    @OneToOne
    @JoinColumn(name = "companydetails_id")
    private CompanyDetailsEntity companyDetailsEntity;
}