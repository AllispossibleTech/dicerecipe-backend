package nl.fontys.dicerecipe.business.impl.converters;

import nl.fontys.dicerecipe.domain.accounts.Consumer;
import nl.fontys.dicerecipe.persistence.Entities.ConsumerEntity;

import java.util.ArrayList;
import java.util.HashSet;

public class ConsumerConverter {
    public static Consumer convert(ConsumerEntity consumerEntity) {
        return Consumer.builder()
                .id(consumerEntity.getId())
                .account(AccountConverter.convert(consumerEntity.getAccountEntity()))
                .doneRecipes(RecipeConverter.convertList(new ArrayList<>(consumerEntity.getDoneRecipeEntities())))
                .foodInventory(ProductConverter.convertList(new ArrayList<>(consumerEntity.getFoodInventoryEntities())))
                .preferredCookingTime(consumerEntity.getPreferredCookingTime())
                .build();
    }

    public static ConsumerEntity convertToEntity(Consumer consumer) {
        return ConsumerEntity.builder()
                .id(consumer.getId())
                .accountEntity(AccountConverter.convertToEntity(consumer.getAccount()))
                .doneRecipeEntities(new HashSet<>(RecipeConverter.convertListToEntities(consumer.getDoneRecipes())))
                .foodInventoryEntities(new HashSet<>(ProductConverter.convertListToEntities(consumer.getFoodInventory())))
                .preferredCookingTime(consumer.getPreferredCookingTime())
                .build();
    }

}
