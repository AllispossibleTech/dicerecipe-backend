package nl.fontys.dicerecipe.business;

import nl.fontys.dicerecipe.domain.accounts.Consumer;
import nl.fontys.dicerecipe.domain.requests.AddRemoveProductToInventoryRequest;
import nl.fontys.dicerecipe.domain.requests.UpdateTimeRequest;

import java.util.Optional;

public interface ConsumersService {
    String createConsumer(Consumer consumer);
    Optional<Consumer> getConsumer(int id);
    String addProductToInventory(AddRemoveProductToInventoryRequest request);
    String removeProductFromInventory(AddRemoveProductToInventoryRequest request);
    String updatePreferredCookingTime(UpdateTimeRequest request);
}
