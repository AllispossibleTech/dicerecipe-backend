package nl.fontys.dicerecipe.business.impl;

import lombok.AllArgsConstructor;
import nl.fontys.dicerecipe.business.ConsumersService;
import nl.fontys.dicerecipe.business.impl.converters.AccountConverter;
import nl.fontys.dicerecipe.business.impl.converters.ConsumerConverter;
import nl.fontys.dicerecipe.domain.accounts.Account;
import nl.fontys.dicerecipe.domain.accounts.Consumer;
import nl.fontys.dicerecipe.domain.accounts.UserType;
import nl.fontys.dicerecipe.domain.requests.AddRemoveProductToInventoryRequest;
import nl.fontys.dicerecipe.domain.requests.UpdateTimeRequest;
import nl.fontys.dicerecipe.persistence.DataAccessImpl.SpringImpl.AccountJPA;
import nl.fontys.dicerecipe.persistence.DataAccessImpl.SpringImpl.ConsumerJPA;
import nl.fontys.dicerecipe.persistence.DataAccessImpl.SpringImpl.ProductJPA;
import nl.fontys.dicerecipe.persistence.Entities.ConsumerEntity;
import nl.fontys.dicerecipe.persistence.Entities.ProductEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Optional;

@Service
@AllArgsConstructor
public class ConsumersServiceImpl implements ConsumersService {
    private final ConsumerJPA consumerRepository;
    private final AccountJPA accountRepository;
    private final ProductJPA productRepository;
    private final PasswordEncoder passwordEncoder;

    @Override
    public String createConsumer(Consumer consumer) {
        if (consumerRepository.existsById(consumer.getId())) {
            return "Consumer with that ID already exists!";
        }
        String encodedPassword = passwordEncoder.encode(consumer.getAccount().getPassword());

        Account newAccount = Account.builder()
                .id(consumer.getAccount().getId())
                .name(consumer.getAccount().getName())
                .email(consumer.getAccount().getEmail())
                .password(encodedPassword)
                .userType(UserType.CONSUMER)
                .build();

        newAccount = AccountConverter.convert(accountRepository.save(AccountConverter.convertToEntity(newAccount)));

        Consumer toSave = consumer.builder()
                .id(consumer.getId())
                .account(newAccount)
                .preferredCookingTime(consumer.getPreferredCookingTime())
                .foodInventory(consumer.getFoodInventory())
                .doneRecipes(consumer.getDoneRecipes())
                .build();

        ConsumerEntity saved = consumerRepository.save(ConsumerConverter.convertToEntity(toSave));
        return "Consumer saved with role id n."+saved.getId();
    }

    @Override
    public Optional<Consumer> getConsumer(int id) {
        return consumerRepository.findById(id).map(ConsumerConverter::convert);
    }

    @Transactional
    @Override
    public String addProductToInventory(AddRemoveProductToInventoryRequest request) {
        if (consumerRepository.existsById(request.getConsumerId()) && productRepository.existsById(request.getProductId())) {

            Optional<ConsumerEntity> optionalConsumer = consumerRepository.findById(request.getConsumerId());
            Optional<ProductEntity> optionalProduct = productRepository.findById(request.getProductId());

            ConsumerEntity consumer = null;
            ProductEntity product = null;

            if (optionalConsumer.isPresent() && optionalProduct.isPresent()) {
                consumer = optionalConsumer.get();
                product = optionalProduct.get();
                consumer.getFoodInventoryEntities().add(product);

                ConsumerEntity saved = consumerRepository.save(consumer);
                return "Updated consumer food inventory now is: " + saved.getFoodInventoryEntities().stream().count();
            }
        }
        return "No update, product not added from inventory";
    }

    @Transactional
    @Override
    public String removeProductFromInventory(AddRemoveProductToInventoryRequest request) {
        if (consumerRepository.existsById(request.getConsumerId()) && productRepository.existsById(request.getProductId())) {

            Optional<ConsumerEntity> optionalConsumer = consumerRepository.findById(request.getConsumerId());
            Optional<ProductEntity> optionalProduct = productRepository.findById(request.getProductId());

            ConsumerEntity consumer = null;
            ProductEntity product = null;

            if (optionalConsumer.isPresent() && optionalProduct.isPresent()) {
                consumer = optionalConsumer.get();
                product = optionalProduct.get();
                ProductEntity productToRemove = product;

                if (consumer != null && consumer.getFoodInventoryEntities().contains(productToRemove))
                {
                    consumer.getFoodInventoryEntities().remove(productToRemove);
                    ConsumerEntity saved = consumerRepository.save(consumer);
                    return "Updated consumer food inventory now is: "+saved.getFoodInventoryEntities().stream().count();
                }
            }
        }
        return "No update, product not removed from inventory";
    }

    @Override
    public String updatePreferredCookingTime(UpdateTimeRequest request) {
        if (consumerRepository.existsById(request.getConsumerId())) {

            Optional<ConsumerEntity> optionalConsumer = consumerRepository.findById(request.getConsumerId());

            ConsumerEntity consumer = null;

            if (optionalConsumer.isPresent()) {
                consumer = optionalConsumer.get();

                if (consumer != null)
                {
                    consumer.setPreferredCookingTime(request.getMinutes());
                    ConsumerEntity saved = consumerRepository.save(consumer);
                    return "Updated preferred cooking time now is: "+saved.getPreferredCookingTime();
                }
            }
        }
        return "No update!";
    }
}
