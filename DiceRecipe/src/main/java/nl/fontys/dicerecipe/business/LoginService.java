package nl.fontys.dicerecipe.business;

import nl.fontys.dicerecipe.domain.login.LoginRequest;
import nl.fontys.dicerecipe.domain.login.LoginResponse;

public interface LoginService {
    LoginResponse login(LoginRequest loginRequest);
}
