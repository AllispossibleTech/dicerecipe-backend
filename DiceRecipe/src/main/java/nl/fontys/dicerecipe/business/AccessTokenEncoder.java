package nl.fontys.dicerecipe.business;

import nl.fontys.dicerecipe.domain.login.AccessToken;

public interface AccessTokenEncoder {
    String encode(AccessToken accessToken);
}
