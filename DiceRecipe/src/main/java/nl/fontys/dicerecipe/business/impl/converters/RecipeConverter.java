package nl.fontys.dicerecipe.business.impl.converters;

import lombok.NoArgsConstructor;
import nl.fontys.dicerecipe.domain.Recipe;
import nl.fontys.dicerecipe.persistence.Entities.RecipeEntity;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

@NoArgsConstructor
public class RecipeConverter {

    public static Recipe convert(RecipeEntity recipeEntity) {
        return Recipe.builder()
                .title(recipeEntity.getTitle())
                .description(recipeEntity.getDescription())
                .timeToPrepare(recipeEntity.getTimeToPrepare())
                .ingredients(ProductConverter.convertList(new ArrayList<>(recipeEntity.getIngredients())))
                .id(recipeEntity.getId())
                .companyDetails(CompanyDetailsConverter.convert(recipeEntity.getCompanyDetailsEntity()))
                .build();
    }

    public static List<Recipe> convertList(List<RecipeEntity> recipeEntities) {
        List<Recipe> result = new ArrayList<Recipe>();
        for (RecipeEntity recipeEntity : recipeEntities) {
            result.add(
                    Recipe.builder()
                            .title(recipeEntity.getTitle())
                            .description(recipeEntity.getDescription())
                            .timeToPrepare(recipeEntity.getTimeToPrepare())
                            .ingredients(ProductConverter.convertList(new ArrayList<>(recipeEntity.getIngredients())))
                            .id(recipeEntity.getId())
                            .companyDetails(CompanyDetailsConverter.convert(recipeEntity.getCompanyDetailsEntity()))
                            .build()
            );
        }
        return result;
    }

    public static List<RecipeEntity> convertListToEntities(List<Recipe> recipes) {
        List<RecipeEntity> result = new ArrayList<RecipeEntity>();
        for (Recipe recipe : recipes) {
            result.add(
                    RecipeEntity.builder()
                            .title(recipe.getTitle())
                            .description(recipe.getDescription())
                            .timeToPrepare(recipe.getTimeToPrepare())
                            .ingredients(new HashSet<>(ProductConverter.convertListToEntities(recipe.getIngredients())))
                            .id(recipe.getId())
                            .companyDetailsEntity(CompanyDetailsConverter.convertToEntity(recipe.getCompanyDetails()))
                            .build()
            );
        }
        return result;
    }

    public static RecipeEntity convertToEntity(Recipe recipe) {
        return RecipeEntity.builder()
                .title(recipe.getTitle())
                .description(recipe.getDescription())
                .timeToPrepare(recipe.getTimeToPrepare())
                .ingredients(new HashSet<>(ProductConverter.convertListToEntities(recipe.getIngredients())))
                .id(recipe.getId())
                .companyDetailsEntity(CompanyDetailsConverter.convertToEntity(recipe.getCompanyDetails()))
                .build();
    }
}
