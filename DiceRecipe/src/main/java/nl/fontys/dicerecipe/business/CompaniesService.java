package nl.fontys.dicerecipe.business;

import nl.fontys.dicerecipe.domain.accounts.Company;
import nl.fontys.dicerecipe.persistence.DataAccessImpl.SpringImpl.CompanyJPA;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

public interface CompaniesService {
    String createCompany(Company company);

    Optional<Company> getCompany(int id);

    List<Company> getCompanies();
}
