package nl.fontys.dicerecipe.business.impl;

import lombok.AllArgsConstructor;
import nl.fontys.dicerecipe.business.ConsumersService;
import nl.fontys.dicerecipe.business.RecipesService;
import nl.fontys.dicerecipe.business.impl.converters.CompanyDetailsConverter;
import nl.fontys.dicerecipe.business.impl.converters.ProductConverter;
import nl.fontys.dicerecipe.business.impl.converters.RecipeConverter;
import nl.fontys.dicerecipe.domain.Product;
import nl.fontys.dicerecipe.domain.Recipe;
import nl.fontys.dicerecipe.domain.accounts.Consumer;
import nl.fontys.dicerecipe.domain.requests.SearchRecipesByRangeRequest;
import nl.fontys.dicerecipe.persistence.DataAccessImpl.SpringImpl.RecipeJPA;
import nl.fontys.dicerecipe.persistence.Entities.RecipeEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class RecipesServiceImpl implements RecipesService {

    private final RecipeJPA recipesRepository;
    private final ConsumersService consumersService;

    @Override
    public List<Recipe> loadSuggestions(int consumerId) {
        List<Recipe> result = new ArrayList<>();
        final Optional<Consumer> consumerOptional = consumersService.getConsumer(consumerId);
        if (!consumerOptional.isEmpty()) {
            for (Recipe recipe : this.getRecipes()) {
                for (Product product : recipe.getIngredients()) {
                    if (consumerOptional.get().getFoodInventory().contains(product)) {
                        result.add(recipe);
                    }
                }
            }
        }
        return result;
    }

    @Override
    public List<Recipe> getRecipes() {
        List<Recipe> recipes = recipesRepository.findAll()
                .stream()
                .map(RecipeConverter::convert)
                .toList();
        return recipes;
    }

    @Override
    public String addRecipe(Recipe recipe) {
        RecipeEntity toSave = RecipeEntity.builder()
                .title(recipe.getTitle())
                .description(recipe.getDescription())
                .timeToPrepare(recipe.getTimeToPrepare())
                .ingredients(new HashSet<>(ProductConverter.convertListToEntities(recipe.getIngredients())))
                .id(recipe.getId())
                .companyDetailsEntity(CompanyDetailsConverter.convertToEntity(recipe.getCompanyDetails()))
                .build();
        RecipeEntity saved = recipesRepository.save(toSave);
        return "Recipe saved with ID n." + saved.getId();
    }

    @Override
    public void delete(Recipe recipe) {
        this.recipesRepository.delete(RecipeConverter.convertToEntity(recipe));
    }

    @Override
    public List<Recipe> getRecipesByCompanyDetailsId(int id) {
        List<Recipe> result = new ArrayList<>();
        for (Recipe r : this.getRecipes())
        {
            if (r.getCompanyDetails().getId() == id)
            {
                result.add(r);
            }
        }
        return result;
    }

    @Override
    public Recipe getRecipeById(int id) {
        final Optional<RecipeEntity> recipeOptional = recipesRepository.findById(id);
        if (!recipeOptional.isEmpty()) {
            return RecipeConverter.convert(recipeOptional.get());
        }
        return null;
    }

    @Override
    public List<Recipe> getRecipesByTimeToPrepare(int timeInMinutes) {
        List<Recipe> result = new ArrayList<>();
        for (Recipe r : this.getRecipes()) {
            if (timeInMinutes >= r.getTimeToPrepare() - 15 && timeInMinutes <= r.getTimeToPrepare() + 15) {
                result.add(r);
            }
        }
        return result;
    }

    @Override
    public List<Recipe> getRecipesByRangeOfTimeToPrepare(SearchRecipesByRangeRequest request) {
        List<RecipeEntity> entities = this.recipesRepository.findByTimeToPrepareRange(request.getMin(), request.getMax());
        List<Recipe> response = RecipeConverter.convertList(entities);
        return response;
    }
}
