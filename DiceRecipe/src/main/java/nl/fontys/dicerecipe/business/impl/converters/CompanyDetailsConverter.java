package nl.fontys.dicerecipe.business.impl.converters;

import lombok.NoArgsConstructor;
import nl.fontys.dicerecipe.domain.CompanyDetails;
import nl.fontys.dicerecipe.persistence.Entities.CompanyDetailsEntity;

@NoArgsConstructor
public class CompanyDetailsConverter {
    public static CompanyDetails convert(CompanyDetailsEntity companyDetails) {
        return CompanyDetails.builder()
                .id(companyDetails.getId())
                .address(companyDetails.getAddress())
                .name(companyDetails.getName())
                .vatNumber(companyDetails.getVatNumber())
                .build();
    }

    public static CompanyDetailsEntity convertToEntity(CompanyDetails companyDetails) {
        return CompanyDetailsEntity.builder()
                .id(companyDetails.getId())
                .address(companyDetails.getAddress())
                .name(companyDetails.getName())
                .vatNumber(companyDetails.getVatNumber())
                .build();
    }
}
