package nl.fontys.dicerecipe.business.impl;

import io.jsonwebtoken.*;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;
import nl.fontys.dicerecipe.business.AccessTokenDecoder;
import nl.fontys.dicerecipe.business.AccessTokenEncoder;
import nl.fontys.dicerecipe.business.exceptions.InvalidAccessTokenException;
import nl.fontys.dicerecipe.domain.login.AccessToken;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.security.Key;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Service
public class AccessTokenEncoderDecoderImpl implements AccessTokenEncoder, AccessTokenDecoder {
    private final Key key;

    public AccessTokenEncoderDecoderImpl(@Value("E91E158E4C6656F68B1B5D1C316766DE98D2AD6EF3BFB44F78E9CFCDF5") String secretKey) {
        byte[] keyBytes = Decoders.BASE64.decode(secretKey);
        this.key = Keys.hmacShaKeyFor(keyBytes);
    }

    @Override
    public String encode(AccessToken accessToken) {
        Map<String, Object> claimsMap = new HashMap<>();
        if (accessToken.getRole() != "" && accessToken.getRole() != null) {
            claimsMap.put("role", accessToken.getRole());
        }
        if (accessToken.getUserId() != null) {
            claimsMap.put("userId", accessToken.getUserId());
        }
        if (accessToken.getRoleId() != null) {
            claimsMap.put("roleId", accessToken.getRoleId());
        }

        Instant now = Instant.now();

        /* NON COMPLIANT CODE!
        return Jwts.builder()
                .setSubject(accessToken.getSubject())
                .setIssuedAt(Date.from(now))
                .setExpiration(Date.from(now.plus(30, ChronoUnit.MINUTES)))
                .addClaims(claimsMap)
                .signWith(key)
                .compact();
         */

        return Jwts.builder()
                .setSubject(accessToken.getSubject())
                .setIssuedAt(Date.from(now))
                .setExpiration(Date.from(now.plus(30, ChronoUnit.MINUTES)))
                .addClaims(claimsMap)
                .signWith(SignatureAlgorithm.HS256, key)
                .compact();
    }

    @Override
    public AccessToken decode(String accessTokenEncoded) {
        try {
            // Jwt jwt = Jwts.parserBuilder().setSigningKey(key).build().parse(accessTokenEncoded); --- Non Compliant
            // Claims claims = (Claims) jwt.getBody();

            Claims claims = Jwts.parser().setSigningKey(key).parseClaimsJws(accessTokenEncoded).getBody();

            String role = claims.get("role", String.class);

            return AccessToken.builder()
                    .subject(claims.getSubject())
                    .role(role)
                    .userId(claims.get("userId", Integer.class))
                    .roleId(claims.get("roleId", Integer.class))
                    .build();
        } catch (JwtException e) {
            throw new InvalidAccessTokenException(e.getMessage());
        }
    }
}