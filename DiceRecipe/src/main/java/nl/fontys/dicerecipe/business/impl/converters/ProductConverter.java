package nl.fontys.dicerecipe.business.impl.converters;

import lombok.NoArgsConstructor;
import nl.fontys.dicerecipe.domain.Product;
import nl.fontys.dicerecipe.persistence.Entities.ProductEntity;

import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
public class ProductConverter {

    public static Product convert(ProductEntity product) {
        return Product.builder()
                .id(product.getId())
                .name(product.getName())
                .unit(product.getUnit())
                .manufacturer(CompanyDetailsConverter.convert(product.getCompanyDetailsEntity()))
                .build();
    }

    public static List<Product> convertList(List<ProductEntity> productEntities)
    {
        List<Product> result = new ArrayList<Product>();
        for (ProductEntity p : productEntities)
        {
            result.add(
                    Product.builder()
                            .id(p.getId())
                            .name(p.getName())
                            .unit(p.getUnit())
                            .manufacturer(CompanyDetailsConverter.convert(p.getCompanyDetailsEntity()))
                            .build()
            );
        }
        return result;
    }

    public static List<ProductEntity> convertListToEntities(List<Product> products)
    {
        List<ProductEntity> result = new ArrayList<ProductEntity>();
        for (Product p : products)
        {
            result.add(
                    ProductEntity.builder()
                            .id(p.getId())
                            .name(p.getName())
                            .unit(p.getUnit())
                            .companyDetailsEntity(CompanyDetailsConverter.convertToEntity(p.getManufacturer()))
                            .build()
            );
        }
        return result;
    }

    public static ProductEntity convertToEntity(Product product) {
        return ProductEntity.builder()
                .id(product.getId())
                .name(product.getName())
                .unit(product.getUnit())
                .companyDetailsEntity(CompanyDetailsConverter.convertToEntity(product.getManufacturer()))
                .build();
    }
}