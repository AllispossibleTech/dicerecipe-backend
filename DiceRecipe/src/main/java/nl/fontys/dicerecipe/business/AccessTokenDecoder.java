package nl.fontys.dicerecipe.business;

import nl.fontys.dicerecipe.domain.login.AccessToken;

public interface AccessTokenDecoder {
    AccessToken decode(String accessTokenEncoded);
}
