package nl.fontys.dicerecipe.business;

import nl.fontys.dicerecipe.domain.Recipe;
import nl.fontys.dicerecipe.domain.requests.SearchRecipesByRangeRequest;

import java.util.List;

public interface RecipesService {
    List<Recipe> loadSuggestions(int consumerId);
    List<Recipe> getRecipes();
    String addRecipe(Recipe recipe);
    void delete(Recipe recipe);
    List<Recipe> getRecipesByCompanyDetailsId(int id);
    Recipe getRecipeById(int id);
    List<Recipe> getRecipesByTimeToPrepare(int timeInMinutes);
    List<Recipe> getRecipesByRangeOfTimeToPrepare(SearchRecipesByRangeRequest request);

}
