package nl.fontys.dicerecipe.business.impl.converters;

import nl.fontys.dicerecipe.domain.accounts.Account;
import nl.fontys.dicerecipe.domain.accounts.UserType;
import nl.fontys.dicerecipe.persistence.Entities.AccountEntity;

import java.util.ArrayList;
import java.util.List;

public class AccountConverter {

    public static Account convert(AccountEntity accountEntity) {
        return Account.builder()
                .id(accountEntity.getId())
                .name(accountEntity.getName())
                .email(accountEntity.getEmail())
                .password(accountEntity.getPassword())
                .userType(UserType.valueOf(accountEntity.getUserType()))
                .build();
    }

    public static List<Account> convertList(List<AccountEntity> accountEntities)
    {
        List<Account> result = new ArrayList<Account>();
        for (AccountEntity accountEntity : accountEntities)
        {
            result.add(
                    Account.builder()
                            .id(accountEntity.getId())
                            .name(accountEntity.getName())
                            .email(accountEntity.getEmail())
                            .password(accountEntity.getPassword())
                            .userType(UserType.valueOf(accountEntity.getUserType()))
                            .build()
            );
        }
        return result;
    }

    public static AccountEntity convertToEntity(Account account)
    {
        return AccountEntity.builder()
                .id(account.getId())
                .name(account.getName())
                .email(account.getEmail())
                .password(account.getPassword())
                .userType(account.getUserType().name())
                .build();
    }
}
