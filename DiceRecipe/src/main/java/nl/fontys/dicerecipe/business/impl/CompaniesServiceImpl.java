package nl.fontys.dicerecipe.business.impl;

import lombok.AllArgsConstructor;
import nl.fontys.dicerecipe.business.CompaniesService;
import nl.fontys.dicerecipe.business.impl.converters.AccountConverter;
import nl.fontys.dicerecipe.business.impl.converters.CompanyConverter;
import nl.fontys.dicerecipe.business.impl.converters.CompanyDetailsConverter;
import nl.fontys.dicerecipe.domain.CompanyDetails;
import nl.fontys.dicerecipe.domain.accounts.Account;
import nl.fontys.dicerecipe.domain.accounts.Company;
import nl.fontys.dicerecipe.domain.accounts.UserType;
import nl.fontys.dicerecipe.persistence.DataAccessImpl.SpringImpl.AccountJPA;
import nl.fontys.dicerecipe.persistence.DataAccessImpl.SpringImpl.CompanyDetailsJPA;
import nl.fontys.dicerecipe.persistence.DataAccessImpl.SpringImpl.CompanyJPA;
import nl.fontys.dicerecipe.persistence.Entities.CompanyEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class CompaniesServiceImpl implements CompaniesService {
    private final CompanyJPA companyRepository;
    private final AccountJPA accountRepository;
    private final CompanyDetailsJPA companyDetailsRepository;
    private final PasswordEncoder passwordEncoder;
    //H
    @Override
    public String createCompany(Company company) {
        if (companyRepository.existsById(company.getId())) {
            return "Company with that ID already exists!";
        }
        String encodedPassword = passwordEncoder.encode(company.getAccount().getPassword());

        Account newAccount = Account.builder()
                .id(company.getAccount().getId())
                .name(company.getAccount().getName())
                .email(company.getAccount().getEmail())
                .password(encodedPassword)
                .userType(UserType.COMPANY)
                .build();

        newAccount = AccountConverter.convert(accountRepository.save(AccountConverter.convertToEntity(newAccount)));

        CompanyDetails newCompanyDetails = CompanyDetailsConverter.convert(companyDetailsRepository.save(CompanyDetailsConverter.convertToEntity(company.getDetails())));

        Company toSave = company.builder()
                .id(company.getId())
                .account(newAccount)
                .details(newCompanyDetails)
                .build();

        CompanyEntity saved = companyRepository.save(CompanyConverter.convertToEntity(toSave));
        return "createCompany method called in service. Saved with id n."+saved.getId();
    }

    public Optional<Company> getCompany(int id) {
        return companyRepository.findById(id).map(CompanyConverter::convert);
    }

    public List<Company> getCompanies() {
        List<Company> companies = companyRepository.findAll()
                .stream()
                .map(CompanyConverter::convert)
                .toList();
        return companies;
    }
}
