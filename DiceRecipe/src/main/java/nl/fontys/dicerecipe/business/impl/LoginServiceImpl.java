package nl.fontys.dicerecipe.business.impl;

import lombok.RequiredArgsConstructor;
import nl.fontys.dicerecipe.business.AccessTokenEncoder;
import nl.fontys.dicerecipe.business.LoginService;
import nl.fontys.dicerecipe.business.exceptions.InvalidCredentialsException;
import nl.fontys.dicerecipe.domain.login.AccessToken;
import nl.fontys.dicerecipe.domain.login.LoginRequest;
import nl.fontys.dicerecipe.domain.login.LoginResponse;
import nl.fontys.dicerecipe.persistence.DataAccessImpl.SpringImpl.AccountJPA;
import nl.fontys.dicerecipe.persistence.DataAccessImpl.SpringImpl.CompanyJPA;
import nl.fontys.dicerecipe.persistence.DataAccessImpl.SpringImpl.ConsumerJPA;
import nl.fontys.dicerecipe.persistence.Entities.AccountEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class LoginServiceImpl implements LoginService {
    private final AccountJPA userRepository;
    private final ConsumerJPA consumerRepository;
    private final CompanyJPA companyRepository;
    private final PasswordEncoder passwordEncoder;
    private final AccessTokenEncoder accessTokenEncoder;

    @Override
    public LoginResponse login(LoginRequest loginRequest) {
        AccountEntity user = userRepository.findByEmail(loginRequest.getEmail()).orElseThrow(InvalidCredentialsException::new);

        //if (user == null) {
        //    throw new InvalidCredentialsException();
        //}

        if (!matchesPassword(loginRequest.getPassword(), user.getPassword())) {
            throw new InvalidCredentialsException();
        }

        String accessToken = generateAccessToken(user);
        return LoginResponse.builder().accessToken(accessToken).build();
    }

    private boolean matchesPassword(String rawPassword, String encodedPassword) {
        return passwordEncoder.matches(rawPassword, encodedPassword);
    }

    private String generateAccessToken(AccountEntity user) {

        int userId = user.getId();
        int roleId = 0;

        if (user.getUserType().equals("CONSUMER")) {
            roleId = this.consumerRepository.findByAccountEntity_Id(userId).getId();
        }
        else if (user.getUserType().equals("COMPANY")) {
            roleId = this.companyRepository.findByAccount_Id(userId).getId();
        }

        return accessTokenEncoder.encode(
                AccessToken.builder()
                        .subject(user.getEmail())
                        .role(user.getUserType())
                        .userId(userId)
                        .roleId(roleId)
                        .build());
    }
}