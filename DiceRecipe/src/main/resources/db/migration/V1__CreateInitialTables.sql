CREATE TABLE usertypes
(
    id int NOT NULL AUTO_INCREMENT,
    type varchar(50) NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE accounts
(
    id int NOT NULL AUTO_INCREMENT,
    name varchar(50) NOT NULL,
    email varchar(50) NOT NULL,
    password varchar(100) NOT NULL,
    usertype varchar(50) NOT NULL,
    PRIMARY KEY (id),
    UNIQUE (email)
);

CREATE TABLE companydetails
(
    id int NOT NULL AUTO_INCREMENT,
    name varchar(50) NOT NULL,
    address varchar(50) NOT NULL,
    vatnumber varchar(50) NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE companies
(
    id int NOT NULL AUTO_INCREMENT,
    account_id int NOT NULL,
    companydetails_id int NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (companydetails_id) REFERENCES companydetails (id),
    FOREIGN KEY (account_id) REFERENCES accounts (id)

);

CREATE TABLE units
(
    id int NOT NULL AUTO_INCREMENT,
    measurement varchar(50) NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE products
(
    id int NOT NULL AUTO_INCREMENT,
    name varchar(50) NOT NULL,
    description varchar(50),
    unit varchar(50) NOT NULL,
    companydetails_id int,
    PRIMARY KEY (id),
    FOREIGN KEY (companydetails_id) REFERENCES companydetails (id)
);

CREATE TABLE recipes
(
    id int NOT NULL AUTO_INCREMENT,
    name varchar(50) NOT NULL,
    description varchar(50) NOT NULL,
    timetoprepare int NOT NULL,
    companydetails_id int,
    PRIMARY KEY (id),
    FOREIGN KEY (companydetails_id) REFERENCES companydetails (id)
);

CREATE TABLE recipe_products
(
    recipe_id int NOT NULL,
    product_id int NOT NULL,
    FOREIGN KEY (recipe_id) REFERENCES recipes (id),
    FOREIGN KEY (product_id) REFERENCES products (id)
);


CREATE TABLE consumers
(
    id int NOT NULL AUTO_INCREMENT,
    account_id int NOT NULL,
    preferredcookingtime int NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (account_id) REFERENCES accounts (id)
);

CREATE TABLE consumer_products
(
    consumer_id int NOT NULL,
    product_id int NOT NULL,
    FOREIGN KEY (consumer_id) REFERENCES consumers (id),
    FOREIGN KEY (product_id) REFERENCES products (id)
);

CREATE TABLE consumer_recipes
(
    consumer_id int NOT NULL,
    recipe_id int NOT NULL,
    FOREIGN KEY (consumer_id) REFERENCES consumers (id),
    FOREIGN KEY (recipe_id) REFERENCES recipes (id)
);


