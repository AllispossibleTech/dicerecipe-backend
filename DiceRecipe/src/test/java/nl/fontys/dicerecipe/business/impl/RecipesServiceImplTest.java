package nl.fontys.dicerecipe.business.impl;

import nl.fontys.dicerecipe.business.ConsumersService;
import nl.fontys.dicerecipe.business.ProductsService;
import nl.fontys.dicerecipe.business.RecipesService;
import nl.fontys.dicerecipe.domain.*;
import nl.fontys.dicerecipe.domain.accounts.Account;
import nl.fontys.dicerecipe.domain.accounts.Consumer;
import nl.fontys.dicerecipe.domain.accounts.UserType;
import nl.fontys.dicerecipe.domain.requests.SearchRecipesByRangeRequest;
import nl.fontys.dicerecipe.persistence.DataAccessImpl.SpringImpl.ProductJPA;
import nl.fontys.dicerecipe.persistence.DataAccessImpl.SpringImpl.RecipeJPA;
import nl.fontys.dicerecipe.persistence.Entities.CompanyDetailsEntity;
import nl.fontys.dicerecipe.persistence.Entities.ProductEntity;
import nl.fontys.dicerecipe.persistence.Entities.RecipeEntity;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class RecipesServiceImplTest {

    private final Product product = Product.builder().id(1).name("Spaghetti").manufacturer(new CompanyDetails(1,"Via Emilia Romagna 1", "Barilla", "IT9302938402")).build();
    private final List<Recipe> recipes = List.of(new Recipe(1,"Carbonara","Cook Spaghetti", 20, List.of(product), new CompanyDetails(1,"Via Emilia Romagna 1", "Barilla", "IT9302938402")));
    private final Consumer consumer = new Consumer(1, new Account("Alessandro Mattiuzzo", "ale@mat.it", "password", UserType.CONSUMER), 40, new ArrayList<Product>(), new ArrayList<Recipe>());

    @Test
    void shouldLoadAllConvertedRecipes() {

        // Arrange
        ProductEntity productEntity = ProductEntity.builder().companyDetailsEntity(CompanyDetailsEntity.builder().id(1).name("Barilla").address("Via Emilia Romagna 1").vatNumber("IT9302938402").build()).id(1).name("Spaghetti").build();
        List<RecipeEntity> recipeEntities = List.of(RecipeEntity.builder().title("Carbonara").description("Cook Spaghetti").timeToPrepare(20).ingredients(Set.of(productEntity)).id(1).companyDetailsEntity(CompanyDetailsEntity.builder().id(1).name("Barilla").address("Via Emilia Romagna 1").vatNumber("IT9302938402").build()).build());

        RecipeJPA recipesRepository = mock(RecipeJPA.class);
        when(recipesRepository.findAll()).thenReturn(recipeEntities);
        ConsumersService consumersService = mock(ConsumersService.class);
        RecipesService recipesService = new RecipesServiceImpl(recipesRepository, consumersService);

        // Act
        List<Recipe> result = recipesService.getRecipes();

        // Assert
        assertEquals(recipes, result);
    }

    @Test
    void shouldLoadAllRecipeSuggestions() {
        // Arrange
        List<Product> inventory = new ArrayList<>();
        inventory.add(product);

        Consumer consumer1 = consumer;
        consumer1.setFoodInventory(List.of(product));

        ProductEntity productEntity = ProductEntity.builder().companyDetailsEntity(CompanyDetailsEntity.builder().id(1).name("Barilla").address("Via Emilia Romagna 1").vatNumber("IT9302938402").build()).id(1).name("Spaghetti").build();
        List<RecipeEntity> recipeEntities = List.of(RecipeEntity.builder().title("Carbonara").description("Cook Spaghetti").timeToPrepare(20).ingredients(Set.of(productEntity)).id(1).companyDetailsEntity(new CompanyDetailsEntity(1,"Barilla", "Via Emilia Romagna 1", "IT9302938402")).build());

        RecipeJPA recipesRepository = mock(RecipeJPA.class);
        when(recipesRepository.findAll()).thenReturn(recipeEntities);

        ConsumersService consumersService = mock(ConsumersService.class);
        when(consumersService.getConsumer(consumer1.getId())).thenReturn(Optional.of(consumer1));

        RecipesService recipesService = new RecipesServiceImpl(recipesRepository, consumersService);

        // Act
        List<Recipe> result = recipesService.loadSuggestions(consumer1.getId());

        // Assert
        assertEquals(recipes, result);
    }

    @Test
    void shouldAddRecipe_WhenDataIsValid() {

        // Arrange
        ProductEntity productEntity = ProductEntity.builder().companyDetailsEntity(CompanyDetailsEntity.builder().id(1).name("Barilla").address("Via Emilia Romagna 1").vatNumber("IT9302938402").build()).id(1).name("Spaghetti").build();
        RecipeEntity recipeEntity = RecipeEntity.builder().title("Carbonara").description("Cook Spaghetti").timeToPrepare(20).ingredients(Set.of(productEntity)).companyDetailsEntity(CompanyDetailsEntity.builder().id(1).name("Barilla").address("Via Emilia Romagna 1").build()).build();
        RecipeEntity savedRecipeEntity = RecipeEntity.builder().title("Carbonara").description("Cook Spaghetti").timeToPrepare(20).ingredients(Set.of(productEntity)).id(1).companyDetailsEntity(CompanyDetailsEntity.builder().id(1).name("Barilla").address("Via Emilia Romagna 1").build()).build();
        Recipe recipe = Recipe.builder().title("Carbonara").description("Cook Spaghetti").timeToPrepare(20).ingredients(List.of(product)).companyDetails(CompanyDetails.builder().id(1).name("Barilla").address("Via Emilia Romagna 1").build()).build();

        RecipeJPA recipesRepository = mock(RecipeJPA.class);
        when(recipesRepository.save(recipeEntity)).thenReturn(savedRecipeEntity);
        ConsumersService consumersService = mock(ConsumersService.class);
        RecipesService recipesService = new RecipesServiceImpl(recipesRepository, consumersService);

        // Act
        String result = recipesService.addRecipe(recipe);

        // Assert
        assertEquals("Recipe saved with ID n.1", result);
    }

    @Test
    void shouldDeleteRecipe_WhenDataIsValid() {

        // Arrange
        ProductEntity productEntity = ProductEntity.builder().companyDetailsEntity(CompanyDetailsEntity.builder().id(1).name("Barilla").address("Via Emilia Romagna 1").vatNumber("IT9302938402").build()).id(1).name("Spaghetti").build();
        RecipeEntity recipeEntity = RecipeEntity.builder().title("Carbonara").description("Cook Spaghetti").timeToPrepare(20).ingredients(Set.of(productEntity)).companyDetailsEntity(CompanyDetailsEntity.builder().id(1).name("Barilla").address("Via Emilia Romagna 1").build()).build();
        RecipeEntity savedRecipeEntity = RecipeEntity.builder().title("Carbonara").description("Cook Spaghetti").timeToPrepare(20).ingredients(Set.of(productEntity)).id(1).companyDetailsEntity(CompanyDetailsEntity.builder().id(1).name("Barilla").address("Via Emilia Romagna 1").build()).build();
        Recipe recipe = Recipe.builder().title("Carbonara").description("Cook Spaghetti").timeToPrepare(20).ingredients(List.of(product)).companyDetails(CompanyDetails.builder().id(1).name("Barilla").address("Via Emilia Romagna 1").build()).build();

        RecipeJPA recipesRepository = mock(RecipeJPA.class);
        doNothing().when(recipesRepository).delete(recipeEntity);
        ConsumersService consumersService = mock(ConsumersService.class);
        RecipesService recipesService = new RecipesServiceImpl(recipesRepository, consumersService);

        // Act
        recipesService.delete(recipe);

        // Assert
        verify(recipesRepository, times(1)).delete(recipeEntity);
    }

    @Test
    void getRecipesByCompanyDetailsId_shouldReturnRecipeList_WhenCompanyDetailsIdMatches() {
        // Arrange
        ProductEntity productEntity = ProductEntity.builder().companyDetailsEntity(CompanyDetailsEntity.builder().id(1).name("Barilla").address("Via Emilia Romagna 1").vatNumber("IT9302938402").build()).id(1).name("Spaghetti").build();
        RecipeEntity recipeEntity = RecipeEntity.builder().title("Carbonara").description("Cook Spaghetti").timeToPrepare(20).ingredients(Set.of(productEntity)).companyDetailsEntity(CompanyDetailsEntity.builder().id(1).name("Barilla").address("Via Emilia Romagna 1").build()).build();
        RecipeEntity savedRecipeEntity = RecipeEntity.builder().title("Carbonara").description("Cook Spaghetti").timeToPrepare(20).ingredients(Set.of(productEntity)).id(1).companyDetailsEntity(CompanyDetailsEntity.builder().id(1).name("Barilla").address("Via Emilia Romagna 1").build()).build();
        Recipe recipe = Recipe.builder().title("Carbonara").description("Cook Spaghetti").timeToPrepare(20).ingredients(List.of(product)).companyDetails(CompanyDetails.builder().id(1).name("Barilla").address("Via Emilia Romagna 1").build()).build();
        RecipeJPA recipesRepository = mock(RecipeJPA.class);
        when(recipesRepository.findAll()).thenReturn(List.of(recipeEntity));
        ConsumersService consumersService = mock(ConsumersService.class);
        RecipesService recipesService = new RecipesServiceImpl(recipesRepository, consumersService);

        // Act
        List<Recipe> result = recipesService.getRecipesByCompanyDetailsId(1);

        // Assert
        assertEquals(List.of(recipe), result);
    }

    @Test
    void getRecipesByTimeToPrepare_shouldReturnRecipeList_WhenTimeMatches() {
        // Arrange
        ProductEntity productEntity = ProductEntity.builder().companyDetailsEntity(CompanyDetailsEntity.builder().id(1).name("Barilla").address("Via Emilia Romagna 1").vatNumber("IT9302938402").build()).id(1).name("Spaghetti").build();
        RecipeEntity recipeEntity = RecipeEntity.builder().title("Carbonara").description("Cook Spaghetti").timeToPrepare(30).ingredients(Set.of(productEntity)).companyDetailsEntity(CompanyDetailsEntity.builder().id(1).name("Barilla").address("Via Emilia Romagna 1").build()).build();
        Recipe recipe = Recipe.builder().title("Carbonara").description("Cook Spaghetti").timeToPrepare(30).ingredients(List.of(product)).companyDetails(CompanyDetails.builder().id(1).name("Barilla").address("Via Emilia Romagna 1").build()).build();
        RecipeJPA recipesRepository = mock(RecipeJPA.class);
        int timeToCompare = 22;
        when(recipesRepository.findAll()).thenReturn(List.of(recipeEntity));
        ConsumersService consumersService = mock(ConsumersService.class);
        RecipesService recipesService = new RecipesServiceImpl(recipesRepository, consumersService);

        // Act
        List<Recipe> result = recipesService.getRecipesByTimeToPrepare(timeToCompare);

        // Assert
        assertEquals(List.of(recipe), result);
    }

    @Test
    void getRecipeById_shouldReturnRecipe_WhenIdMatches() {
        // Arrange
        ProductEntity productEntity = ProductEntity.builder().companyDetailsEntity(CompanyDetailsEntity.builder().id(1).name("Barilla").address("Via Emilia Romagna 1").vatNumber("IT9302938402").build()).id(1).name("Spaghetti").build();
        RecipeEntity recipeEntity = RecipeEntity.builder().id(1).title("Carbonara").description("Cook Spaghetti").timeToPrepare(20).ingredients(Set.of(productEntity)).companyDetailsEntity(CompanyDetailsEntity.builder().id(1).name("Barilla").address("Via Emilia Romagna 1").vatNumber("IT9302938402").build()).build();
        Recipe recipe = Recipe.builder().id(1).title("Carbonara").description("Cook Spaghetti").timeToPrepare(20).ingredients(List.of(product)).companyDetails(CompanyDetails.builder().id(1).name("Barilla").address("Via Emilia Romagna 1").vatNumber("IT9302938402").build()).build();
        RecipeJPA recipesRepository = mock(RecipeJPA.class);
        when(recipesRepository.findById(1)).thenReturn(Optional.of(recipeEntity));
        ConsumersService consumersService = mock(ConsumersService.class);
        RecipesService recipesService = new RecipesServiceImpl(recipesRepository, consumersService);

        // Act
        Recipe result = recipesService.getRecipeById(1);

        // Assert
        assertEquals(recipe, result);
    }

    @Test
    void getRecipesByRangeOfTimeToPrepare_shouldReturnRecipeList_WhenTimeRangeMatches() {
        // Arrange
        ProductEntity productEntity = ProductEntity.builder().companyDetailsEntity(CompanyDetailsEntity.builder().id(1).name("Barilla").address("Via Emilia Romagna 1").vatNumber("IT9302938402").build()).id(1).name("Spaghetti").build();
        RecipeEntity recipeEntity = RecipeEntity.builder().title("Carbonara").description("Cook Spaghetti").timeToPrepare(30).ingredients(Set.of(productEntity)).companyDetailsEntity(CompanyDetailsEntity.builder().id(1).name("Barilla").address("Via Emilia Romagna 1").build()).build();
        Recipe recipe = Recipe.builder().title("Carbonara").description("Cook Spaghetti").timeToPrepare(30).ingredients(List.of(product)).companyDetails(CompanyDetails.builder().id(1).name("Barilla").address("Via Emilia Romagna 1").build()).build();
        RecipeJPA recipesRepository = mock(RecipeJPA.class);
        SearchRecipesByRangeRequest request = SearchRecipesByRangeRequest.builder().max(5).min(500).build();
        when(recipesRepository.findByTimeToPrepareRange(request.getMin(), request.getMax())).thenReturn(List.of(recipeEntity));
        ConsumersService consumersService = mock(ConsumersService.class);
        RecipesService recipesService = new RecipesServiceImpl(recipesRepository, consumersService);

        // Act
        List<Recipe> result = recipesService.getRecipesByRangeOfTimeToPrepare(request);

        // Assert
        assertEquals(List.of(recipe), result);
    }
}