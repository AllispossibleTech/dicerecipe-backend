package nl.fontys.dicerecipe.business.impl;

import nl.fontys.dicerecipe.business.CompaniesService;
import nl.fontys.dicerecipe.business.ProductsService;
import nl.fontys.dicerecipe.domain.CompanyDetails;
import nl.fontys.dicerecipe.domain.Product;
import nl.fontys.dicerecipe.domain.accounts.Account;
import nl.fontys.dicerecipe.domain.accounts.Company;
import nl.fontys.dicerecipe.domain.accounts.UserType;
import nl.fontys.dicerecipe.domain.requests.CreateProductRequest;
import nl.fontys.dicerecipe.persistence.DataAccessImpl.SpringImpl.ProductJPA;
import nl.fontys.dicerecipe.persistence.Entities.CompanyDetailsEntity;
import nl.fontys.dicerecipe.persistence.Entities.ProductEntity;
import org.junit.jupiter.api.Test;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

class ProductsServiceImplTest {

    private CompaniesService companiesService = mock(CompaniesService.class);
    private final ProductEntity productEntity = ProductEntity.builder().companyDetailsEntity(CompanyDetailsEntity.builder().id(1).name("Barilla").address("Via Emilia Romagna 1").vatNumber("IT9302938402").build()).id(1).name("Spaghetti").build();
    private final Product product = Product.builder().manufacturer(CompanyDetails.builder().id(1).name("Barilla").address("Via Emilia Romagna 1").vatNumber("IT9302938402").build()).id(1).name("Spaghetti").build();

    @Test
    void shouldLoadAllConvertedProducts() {

        // Arrange
        List<Product> products = List.of(product);
        List<ProductEntity> productEntities = List.of(productEntity);

        ProductJPA productRepository = mock(ProductJPA.class);
        when(productRepository.findAll()).thenReturn(productEntities);
        ProductsService productsService = new ProductsServiceImpl(productRepository, companiesService);

        // Act
        List<Product> result = productsService.getProducts();

        // Assert
        assertEquals(products, result);
    }

    @Test
    void shouldCreateProduct_WhenDataIsValid() {

        // Arrange
        ProductJPA productRepository = mock(ProductJPA.class);
        Company company = Company.builder().account(Account.builder().id(1).name("Barilla").email("admin@barilla.it").userType(UserType.COMPANY).build()).id(1).details(CompanyDetails.builder().id(1).name("Barilla").address("Via Emilia Romagna 1").vatNumber("IT9302938402").build()).build();
        when(productRepository.save(productEntity)).thenReturn(productEntity);
        when(companiesService.getCompany(1)).thenReturn(Optional.of(company));
        ProductsService productsService = new ProductsServiceImpl(productRepository, companiesService);
        CreateProductRequest request = CreateProductRequest.builder().product(product).companyId(1).build();
        // Act
        String result = productsService.createProduct(request);

        // Assert
        assertEquals("Product saved with id n.1", result);
    }

    @Test
    void shouldNotCreateProduct_WhenDataIsNotValid() {

        // Arrange
        ProductJPA productRepository = mock(ProductJPA.class);
        Company company = Company.builder().account(Account.builder().id(1).name("Barilla").email("admin@barilla.it").userType(UserType.COMPANY).build()).id(1).details(CompanyDetails.builder().id(1).name("Barilla").address("Via Emilia Romagna 1").vatNumber("IT9302938402").build()).build();
        when(productRepository.save(productEntity)).thenReturn(productEntity);
        when(companiesService.getCompany(1)).thenReturn(Optional.empty());
        ProductsService productsService = new ProductsServiceImpl(productRepository, companiesService);
        CreateProductRequest request = CreateProductRequest.builder().product(product).companyId(1).build();
        // Act
        String result = productsService.createProduct(request);

        // Assert
        assertEquals("Error: Company with that Id not found!", result);
    }

    @Test
    void delete_shouldReturnSuccessMessage_WhenProductExistsAndCanBeDeleted() {
        // Arrange
        ProductJPA productRepository = mock(ProductJPA.class);
        ProductsService productsService = new ProductsServiceImpl(productRepository, companiesService);

        when(productRepository.existsById(product.getId())).thenReturn(true);
        doNothing().when(productRepository).delete(productEntity);

        // Act
        String result = productsService.delete(product);

        // Assert
        assertEquals("Product exists and is going to be deleted!", result);
        verify(productRepository, times(1)).existsById(product.getId());
        verify(productRepository, times(1)).delete(any(ProductEntity.class));
    }

    @Test
    void delete_shouldReturnErrorMessage_WhenProductExistsButCannotBeDeleted() {
        // Arrange
        ProductJPA productRepository = mock(ProductJPA.class);
        ProductsService productsService = new ProductsServiceImpl(productRepository, companiesService);

        when(productRepository.existsById(product.getId())).thenReturn(true);
        doThrow(new RuntimeException("Product cannot be deleted because it's part of a Recipe as an ingredient!")).when(productRepository).delete(productEntity);

        // Act
        String result = productsService.delete(product);

        // Assert
        assertEquals("Product cannot be deleted because it's part of a Recipe as an ingredient!", result);
        verify(productRepository, times(1)).existsById(product.getId());
        verify(productRepository, times(1)).delete(any(ProductEntity.class));
    }

    @Test
    void delete_shouldReturnProductNotFoundMessage_WhenProductDoesNotExist() {
        // Arrange
        int productId = 1;
        ProductJPA productRepository = mock(ProductJPA.class);
        Product product = Product.builder().id(productId).build();
        ProductsService productsService = new ProductsServiceImpl(productRepository, companiesService);

        when(productRepository.existsById(productId)).thenReturn(false);

        // Act
        String result = productsService.delete(product);

        // Assert
        assertEquals("Product does not exist", result);
        verify(productRepository, times(1)).existsById(productId);
        verify(productRepository, never()).delete(any(ProductEntity.class));
    }

    @Test
    void search_shouldReturnProductList_WhenProductNameMatches() {
        // Arrange
        ProductJPA productRepository = mock(ProductJPA.class);
        when(productRepository.findAll()).thenReturn(List.of(productEntity));
        ProductsService productsService = new ProductsServiceImpl(productRepository, companiesService);

        // Act
        List<Product> result = productsService.search("Spaghetti");

        // Assert
        assertEquals(List.of(product), result);
    }

    @Test
    void getProductsByCompanyDetailsId_shouldReturnProductList_WhenCompanyDetailsIdMatches() {
        // Arrange
        ProductJPA productRepository = mock(ProductJPA.class);
        when(productRepository.findAll()).thenReturn(List.of(productEntity));
        ProductsService productsService = new ProductsServiceImpl(productRepository, companiesService);

        // Act
        List<Product> result = productsService.getProductsByCompanyDetailsId(1);

        // Assert
        assertEquals(List.of(product), result);
    }
}