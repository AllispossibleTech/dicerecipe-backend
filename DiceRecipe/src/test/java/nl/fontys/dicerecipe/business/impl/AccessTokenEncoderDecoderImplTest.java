package nl.fontys.dicerecipe.business.impl;

import io.jsonwebtoken.security.Keys;
import nl.fontys.dicerecipe.business.exceptions.InvalidAccessTokenException;
import nl.fontys.dicerecipe.domain.login.AccessToken;
import org.junit.jupiter.api.Test;

import java.security.Key;


import static org.junit.jupiter.api.Assertions.*;

class AccessTokenEncoderDecoderImplTest {

    private String encodedToken = "";
    private final String invalidToken = "blabla.blabla.blabla";

    @Test
    void testEncode() {
        // Arrange
        AccessTokenEncoderDecoderImpl accessTokenEncoderDecoder = new AccessTokenEncoderDecoderImpl("E91E158E4C6656F68B1B5D1C316766DE98D2AD6EF3BFB44F78E9CFCDF5");
        AccessToken accessToken = AccessToken.builder()
                .subject("test_subject")
                .role("test_role")
                .userId(123)
                .roleId(456)
                .build();

        // Act
        this.encodedToken = accessTokenEncoderDecoder.encode(accessToken);

        // Assert
        assertNotNull(encodedToken);
        System.out.println(encodedToken);
    }

    @Test
    void testDecode_InvalidToken_ThrowsInvalidAccessTokenException() {
        // Arrange
        AccessTokenEncoderDecoderImpl accessTokenEncoderDecoder = new AccessTokenEncoderDecoderImpl("E91E158E4C6656F68B1B5D1C316766DE98D2AD6EF3BFB44F78E9CFCDF5");
        String invalidToken = "eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ0ZXN0X3N1YmplY3QiLCJpYXQiOjE2ODQzMzcwMjgsImV4cCI6MTY4NDMzODgyOCwicm9sZSI6InRlc3Rfcm9sZSIsI.sxk2w88dHX_kdPCDdE7rN75_rhMO4c8PP5Z6s5KWftA";

        // Mock Jwts.parser().setSigningKey(key).parseClaimsJws(accessTokenEncoded).getBody()
        Key key = Keys.hmacShaKeyFor("E91E158E4C6656F68B1B5D1C316766DE98D2AD6EF3BFB44F78E9CFCDF5".getBytes());
        // CANNOT MOCK Jwts BECAUSE IT IS A FINAL CLASS. when(Jwts.parser().setSigningKey(key).parseClaimsJws(invalidToken)).thenThrow(JwtException.class);

        // Act and Assert
        assertThrows(InvalidAccessTokenException.class, () -> accessTokenEncoderDecoder.decode(invalidToken));
    }

}