package nl.fontys.dicerecipe.business.impl;

import nl.fontys.dicerecipe.business.AccessTokenEncoder;
import nl.fontys.dicerecipe.business.LoginService;
import nl.fontys.dicerecipe.business.exceptions.InvalidCredentialsException;
import nl.fontys.dicerecipe.domain.login.AccessToken;
import nl.fontys.dicerecipe.domain.login.LoginRequest;
import nl.fontys.dicerecipe.domain.login.LoginResponse;
import nl.fontys.dicerecipe.persistence.DataAccessImpl.SpringImpl.AccountJPA;
import nl.fontys.dicerecipe.persistence.DataAccessImpl.SpringImpl.CompanyJPA;
import nl.fontys.dicerecipe.persistence.DataAccessImpl.SpringImpl.ConsumerJPA;
import nl.fontys.dicerecipe.persistence.Entities.*;
import org.junit.jupiter.api.Test;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.HashSet;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

class LoginServiceImplTest {

    AccountJPA accountRepository = mock(AccountJPA.class);
    CompanyJPA companyRepository = mock(CompanyJPA.class);
    ConsumerJPA consumerRepository = mock(ConsumerJPA.class);
    AccessTokenEncoder accessTokenEncoder = mock(AccessTokenEncoder.class);
    PasswordEncoder passwordEncoder = mock(PasswordEncoder.class);
    LoginService loginService = new LoginServiceImpl(accountRepository, consumerRepository, companyRepository, passwordEncoder, accessTokenEncoder);

    private final AccountEntity foundAccount = AccountEntity.builder().id(1).name("Mario").email("mario@gmail.com").userType("CONSUMER").build();
    private final AccountEntity foundCompanyAccount = AccountEntity.builder().id(1).name("Barilla").email("admin@barilla.it").userType("COMPANY").build();
    private final CompanyEntity foundCompany = CompanyEntity.builder().account(foundCompanyAccount).id(1).build();
    private final ConsumerEntity foundCounsumer = ConsumerEntity.builder().id(1).accountEntity(foundAccount).doneRecipeEntities(new HashSet<RecipeEntity>()).foodInventoryEntities(new HashSet<ProductEntity>()).preferredCookingTime(30).build();
    private final String accessToken = "Bearer 123";

    @Test
    public void loginShouldReturnToken_WhenConsumerDataIsValid(){
        LoginRequest loginRequest = new LoginRequest("mario@gmail.com", "mario");
        when(accountRepository.findByEmail(loginRequest.getEmail())).thenReturn(Optional.of(foundAccount));
        when(consumerRepository.findByAccountEntity_Id(1)).thenReturn(foundCounsumer);
        when(passwordEncoder.matches(loginRequest.getPassword(),
                foundAccount.getPassword())).thenReturn(true);
        when(accessTokenEncoder.encode(any(AccessToken.class))).thenReturn(accessToken);
        // Act
        LoginResponse loginResponse = loginService.login((loginRequest));
        // Assert
        verify(accountRepository).findByEmail(loginRequest.getEmail());
        assertNotNull(loginResponse);
        assertEquals(accessToken, loginResponse.getAccessToken());
    }

    @Test
    public void loginShouldReturnToken_WhenCompanyDataIsValid(){
        LoginRequest loginRequest = new LoginRequest("admin@barilla.it", "barilla");
        when(accountRepository.findByEmail(loginRequest.getEmail())).thenReturn(Optional.of(foundCompanyAccount));
        when(companyRepository.findByAccount_Id(1)).thenReturn(foundCompany);
        when(passwordEncoder.matches(loginRequest.getPassword(),
                foundAccount.getPassword())).thenReturn(true);
        when(accessTokenEncoder.encode(any(AccessToken.class))).thenReturn(accessToken);
        // Act
        LoginResponse loginResponse = loginService.login((loginRequest));
        // Assert
        verify(accountRepository).findByEmail(loginRequest.getEmail());
        assertNotNull(loginResponse);
        assertEquals(accessToken, loginResponse.getAccessToken());
    }

    @Test
    public void loginShouldThrowException_WhenUserIsNotFound(){
        // Arrange
        LoginRequest badRequest = new LoginRequest("mario@gmail.com",
                "mario");
        when(accountRepository.findByEmail(badRequest.getEmail())).thenReturn(Optional.empty());

        // Act
        Throwable thrown = assertThrows(InvalidCredentialsException.class,
                () -> loginService.login(badRequest));
        // Assert
        assertNotNull(thrown);
        assertEquals("400 BAD_REQUEST \"INVALID_CREDENTIALS\"", thrown.getMessage());
    }

    @Test
    public void loginShouldThrowException_WhenPasswordIsIncorrect(){
        // Arrange
        LoginRequest badRequest = new LoginRequest("mario@gmail.com",
                "alessandro");
        when(accountRepository.findByEmail(badRequest.getEmail())).thenReturn(Optional.of(foundAccount));
        when(passwordEncoder.matches(badRequest.getPassword(),
                foundAccount.getPassword())).thenReturn(false);
        when(accessTokenEncoder.encode(any(AccessToken.class))).thenReturn(accessToken);
        // Act
        Throwable thrown = assertThrows(InvalidCredentialsException.class,
                () -> loginService.login(badRequest));
        // Assert
        assertNotNull(thrown);
        assertEquals("400 BAD_REQUEST \"INVALID_CREDENTIALS\"", thrown.getMessage());
    }
}