package nl.fontys.dicerecipe.business.impl;

import nl.fontys.dicerecipe.business.ConsumersService;
import nl.fontys.dicerecipe.domain.Product;
import nl.fontys.dicerecipe.domain.Recipe;
import nl.fontys.dicerecipe.domain.accounts.Account;
import nl.fontys.dicerecipe.domain.accounts.Consumer;
import nl.fontys.dicerecipe.domain.accounts.UserType;
import nl.fontys.dicerecipe.domain.requests.AddRemoveProductToInventoryRequest;
import nl.fontys.dicerecipe.persistence.DataAccessImpl.SpringImpl.AccountJPA;
import nl.fontys.dicerecipe.persistence.DataAccessImpl.SpringImpl.ConsumerJPA;
import nl.fontys.dicerecipe.persistence.DataAccessImpl.SpringImpl.ProductJPA;
import nl.fontys.dicerecipe.persistence.Entities.*;
import org.junit.jupiter.api.Test;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class ConsumersServiceImplTest {

    private final ConsumerJPA consumerRepository = mock(ConsumerJPA.class);
    private final AccountJPA accountRepository = mock(AccountJPA.class);
    private final ProductJPA productRepository = mock(ProductJPA.class);
    private final PasswordEncoder passwordEncoder = mock(PasswordEncoder.class);
    private final ConsumersService consumerService = new ConsumersServiceImpl(consumerRepository, accountRepository, productRepository, passwordEncoder);


    @Test
    void createConsumer_whenConsumerDoesNotExist() {
        // Arrange
        final AccountEntity accountEntity = AccountEntity.builder().id(1).name("Mario").email("mario@gmail.com").userType("CONSUMER").build();
        final ConsumerEntity consumerEntity = ConsumerEntity.builder().id(1).accountEntity(accountEntity).doneRecipeEntities(new HashSet<RecipeEntity>()).foodInventoryEntities(new HashSet<ProductEntity>()).preferredCookingTime(30).build();
        final Account account = Account.builder().id(1).name("Mario").email("mario@gmail.com").userType(UserType.CONSUMER).build();
        final Consumer consumer = Consumer.builder().id(1).account(account).doneRecipes(new ArrayList<Recipe>()).foodInventory(new ArrayList<Product>()).preferredCookingTime(30).build();
        when(consumerRepository.existsById(consumer.getId())).thenReturn(false);
        when(accountRepository.save(accountEntity)).thenReturn(accountEntity);
        when(consumerRepository.save(consumerEntity)).thenReturn(consumerEntity);

        // Act
        String result = this.consumerService.createConsumer(consumer);

        // Assert
        assertEquals("Consumer saved with role id n.1", result);
    }

    @Test
    void createConsumer_whenConsumerAlreadyExists() {
        // Arrange
        final AccountEntity accountEntity = AccountEntity.builder().id(1).name("Mario").email("mario@gmail.com").userType("CONSUMER").build();
        final ConsumerEntity consumerEntity = ConsumerEntity.builder().id(1).accountEntity(accountEntity).doneRecipeEntities(new HashSet<RecipeEntity>()).foodInventoryEntities(new HashSet<ProductEntity>()).preferredCookingTime(30).build();
        final Account account = Account.builder().id(1).name("Mario").email("mario@gmail.com").userType(UserType.CONSUMER).build();
        final Consumer consumer = Consumer.builder().id(1).account(account).doneRecipes(new ArrayList<Recipe>()).foodInventory(new ArrayList<Product>()).preferredCookingTime(30).build();
        when(consumerRepository.existsById(consumer.getId())).thenReturn(true);
        when(accountRepository.save(accountEntity)).thenReturn(accountEntity);
        when(consumerRepository.save(consumerEntity)).thenReturn(consumerEntity);

        // Act
        String result = this.consumerService.createConsumer(consumer);
        //Throwable thrown = assertThrows(UserAlreadyExistsException.class,
        //        () -> this.consumerService.createConsumer(consumer));

        // Assert
        assertEquals("Consumer with that ID already exists!", result);
    }

    @Test
    void getConsumer() {
        // Arrange
        final AccountEntity accountEntity = AccountEntity.builder().id(1).name("Mario").email("mario@gmail.com").userType("CONSUMER").build();
        final ConsumerEntity consumerEntity = ConsumerEntity.builder().id(1).accountEntity(accountEntity).doneRecipeEntities(new HashSet<RecipeEntity>()).foodInventoryEntities(new HashSet<ProductEntity>()).preferredCookingTime(30).build();
        final Account account = Account.builder().id(1).name("Mario").email("mario@gmail.com").userType(UserType.CONSUMER).build();
        final Consumer consumer = Consumer.builder().id(1).account(account).doneRecipes(new ArrayList<Recipe>()).foodInventory(new ArrayList<Product>()).preferredCookingTime(30).build();
        when(consumerRepository.findById(consumerEntity.getId())).thenReturn(Optional.of(consumerEntity));

        // Act
        Optional<Consumer> result = this.consumerService.getConsumer(1);

        // Assert
        assertEquals(Optional.of(consumer), result);
    }

    @Test
    void addProductToInventory() {
        // Arrange
        final AddRemoveProductToInventoryRequest request = AddRemoveProductToInventoryRequest.builder().productId(1).consumerId(1).build();
        final AccountEntity accountEntity = AccountEntity.builder().id(1).name("Mario").email("mario@gmail.com").userType("CONSUMER").build();
        final ProductEntity productEntity = ProductEntity.builder().companyDetailsEntity(CompanyDetailsEntity.builder().id(1).name("Barilla").address("Via Emilia Romagna 1").vatNumber("IT9302938402").build()).id(1).name("Spaghetti").build();
        Set<ProductEntity> inventory = new HashSet<ProductEntity>();
        inventory.add(productEntity);
        final ConsumerEntity updatedConsumerEntity = ConsumerEntity.builder().id(1).accountEntity(accountEntity).doneRecipeEntities(new HashSet<RecipeEntity>()).foodInventoryEntities(inventory).preferredCookingTime(30).build();
        final ConsumerEntity consumerEntity = ConsumerEntity.builder().id(1).accountEntity(accountEntity).doneRecipeEntities(new HashSet<RecipeEntity>()).foodInventoryEntities(new HashSet<ProductEntity>()).preferredCookingTime(30).build();
        when(consumerRepository.existsById(request.getConsumerId())).thenReturn(true);
        when(productRepository.existsById(request.getProductId())).thenReturn(true);
        when(consumerRepository.findById(consumerEntity.getId())).thenReturn(Optional.of(consumerEntity));
        when(productRepository.findById(request.getProductId())).thenReturn(Optional.of(productEntity));
        when(consumerRepository.save(updatedConsumerEntity)).thenReturn(updatedConsumerEntity);

        // Act
        String result = this.consumerService.addProductToInventory(request);

        // Assert
        assertEquals("Updated consumer food inventory now is: 1", result);
    }

    @Test
    void addProductToInventory_WhenProductOrConsumerDoNotExist() {
        // Arrange
        final AddRemoveProductToInventoryRequest request = AddRemoveProductToInventoryRequest.builder().productId(1).consumerId(1).build();
        when(consumerRepository.existsById(request.getConsumerId())).thenReturn(false);
        when(productRepository.existsById(request.getProductId())).thenReturn(false);
        when(consumerRepository.findById(request.getConsumerId())).thenReturn(Optional.empty());
        when(productRepository.findById(request.getProductId())).thenReturn(Optional.empty());

        // Act
        String result = this.consumerService.addProductToInventory(request);

        // Assert
        assertEquals("No update, product not added from inventory", result);
    }

    @Test
    void removeProductFromInventory_WhenProductOrConsumerDoNotExist() {
        // Arrange
        final AddRemoveProductToInventoryRequest request = AddRemoveProductToInventoryRequest.builder().productId(1).consumerId(1).build();
        when(consumerRepository.existsById(request.getConsumerId())).thenReturn(false);
        when(productRepository.existsById(request.getProductId())).thenReturn(false);
        when(consumerRepository.findById(request.getConsumerId())).thenReturn(Optional.empty());
        when(productRepository.findById(request.getProductId())).thenReturn(Optional.empty());

        // Act
        String result = this.consumerService.removeProductFromInventory(request);

        // Assert
        assertEquals("No update, product not removed from inventory", result);
    }

    @Test
    void removeProductFromInventory() {
        // Arrange
        final AddRemoveProductToInventoryRequest request = AddRemoveProductToInventoryRequest.builder().productId(1).consumerId(1).build();
        final AccountEntity accountEntity = AccountEntity.builder().id(1).name("Mario").email("mario@gmail.com").userType("CONSUMER").build();
        final ProductEntity productEntity = ProductEntity.builder().companyDetailsEntity(CompanyDetailsEntity.builder().id(1).name("Barilla").address("Via Emilia Romagna 1").vatNumber("IT9302938402").build()).id(1).name("Spaghetti").build();
        Set<ProductEntity> inventory = new HashSet<ProductEntity>();
        inventory.add(productEntity);
        final ConsumerEntity consumerEntity = ConsumerEntity.builder().id(1).accountEntity(accountEntity).doneRecipeEntities(new HashSet<RecipeEntity>()).foodInventoryEntities(inventory).preferredCookingTime(30).build();
        final ConsumerEntity updatedConsumerEntity = ConsumerEntity.builder().id(1).accountEntity(accountEntity).doneRecipeEntities(new HashSet<RecipeEntity>()).foodInventoryEntities(new HashSet<ProductEntity>()).preferredCookingTime(30).build();
        when(consumerRepository.existsById(request.getConsumerId())).thenReturn(true);
        when(productRepository.existsById(request.getProductId())).thenReturn(true);
        when(consumerRepository.findById(consumerEntity.getId())).thenReturn(Optional.of(consumerEntity));
        when(productRepository.findById(request.getProductId())).thenReturn(Optional.of(productEntity));
        when(consumerRepository.save(updatedConsumerEntity)).thenReturn(updatedConsumerEntity);

        // Act
        String result = this.consumerService.removeProductFromInventory(request);

        // Assert
        assertEquals("Updated consumer food inventory now is: 0", result);
    }
}