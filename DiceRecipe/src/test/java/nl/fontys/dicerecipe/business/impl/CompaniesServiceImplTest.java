package nl.fontys.dicerecipe.business.impl;

import nl.fontys.dicerecipe.business.CompaniesService;
import nl.fontys.dicerecipe.domain.CompanyDetails;
import nl.fontys.dicerecipe.domain.accounts.Account;
import nl.fontys.dicerecipe.domain.accounts.Company;
import nl.fontys.dicerecipe.domain.accounts.UserType;
import nl.fontys.dicerecipe.persistence.DataAccessImpl.SpringImpl.*;
import nl.fontys.dicerecipe.persistence.Entities.*;
import org.junit.jupiter.api.Test;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class CompaniesServiceImplTest {

    // private final ProductJPA productRepository = mock(ProductJPA.class);
    private final CompanyJPA companyRepository = mock(CompanyJPA.class);
    private final AccountJPA accountRepository = mock(AccountJPA.class);
    private final CompanyDetailsJPA companyDetailsRepository = mock(CompanyDetailsJPA.class);
    private final PasswordEncoder passwordEncoder = mock(PasswordEncoder.class);
    private final CompaniesService companiesService = new CompaniesServiceImpl(companyRepository, accountRepository, companyDetailsRepository, passwordEncoder);

    @Test
    void createCompany_whenCompanyDoesNotExist() {
        // Arrange
        final AccountEntity accountEntity = AccountEntity.builder().id(1).name("Barilla").email("admin@barilla.it").userType("COMPANY").build();
        final Account account = Account.builder().id(1).name("Barilla").email("admin@barilla.it").userType(UserType.COMPANY).build();
        final Company company = Company.builder().account(account).id(1).details(CompanyDetails.builder().id(1).build()).build();
        final CompanyDetailsEntity companyDetailsEntity = CompanyDetailsEntity.builder().id(1).build();
        final CompanyEntity companyEntity = CompanyEntity.builder().account(accountEntity).id(1).companyDetailsEntity(companyDetailsEntity).build();
        when(companyRepository.existsById(company.getId())).thenReturn(false);
        when(accountRepository.save(accountEntity)).thenReturn(accountEntity);
        when(companyDetailsRepository.save(companyDetailsEntity)).thenReturn(companyDetailsEntity);
        when(companyRepository.save(companyEntity)).thenReturn(companyEntity);

        // Act
        String result = this.companiesService.createCompany(company);

        // Assert
        assertEquals("createCompany method called in service. Saved with id n.1", result);
    }

    @Test
    void createCompany_whenCompanyAlreadyExists() {
        // Arrange
        final AccountEntity accountEntity = AccountEntity.builder().id(1).name("Barilla").email("admin@barilla.it").userType("COMPANY").build();
        final Account account = Account.builder().id(1).name("Barilla").email("admin@barilla.it").userType(UserType.COMPANY).build();
        final Company company = Company.builder().account(account).id(1).details(CompanyDetails.builder().id(1).build()).build();
        final CompanyDetailsEntity companyDetailsEntity = CompanyDetailsEntity.builder().id(1).build();
        final CompanyEntity companyEntity = CompanyEntity.builder().account(accountEntity).id(1).companyDetailsEntity(companyDetailsEntity).build();
        when(companyRepository.existsById(company.getId())).thenReturn(true);
        when(accountRepository.save(accountEntity)).thenReturn(accountEntity);
        when(companyDetailsRepository.save(companyDetailsEntity)).thenReturn(companyDetailsEntity);
        when(companyRepository.save(companyEntity)).thenReturn(companyEntity);

        // Act
        String result = this.companiesService.createCompany(company);

        // Assert
        assertEquals("Company with that ID already exists!", result);
    }

    @Test
    void getCompany() {
        // Arrange
        final AccountEntity accountEntity = AccountEntity.builder().id(1).name("Barilla").email("admin@barilla.it").userType("COMPANY").build();
        final Account account = Account.builder().id(1).name("Barilla").email("admin@barilla.it").userType(UserType.COMPANY).build();
        final Company company = Company.builder().account(account).id(1).details(CompanyDetails.builder().id(1).build()).build();
        final CompanyDetailsEntity companyDetailsEntity = CompanyDetailsEntity.builder().id(1).build();
        final CompanyEntity companyEntity = CompanyEntity.builder().account(accountEntity).id(1).companyDetailsEntity(companyDetailsEntity).build();
        when(companyRepository.findById(1)).thenReturn(Optional.of(companyEntity));

        // Act
        Optional<Company> result = this.companiesService.getCompany(1);

        // Assert
        assertEquals(Optional.of(company), result);
    }

    @Test
    void getCompanies() {
        // Arrange
        final AccountEntity accountEntity = AccountEntity.builder().id(1).name("Barilla").email("admin@barilla.it").userType("COMPANY").build();
        final Account account = Account.builder().id(1).name("Barilla").email("admin@barilla.it").userType(UserType.COMPANY).build();
        final Company company = Company.builder().account(account).id(1).details(CompanyDetails.builder().id(1).build()).build();
        final CompanyDetailsEntity companyDetailsEntity = CompanyDetailsEntity.builder().id(1).build();
        final CompanyEntity companyEntity = CompanyEntity.builder().account(accountEntity).id(1).companyDetailsEntity(companyDetailsEntity).build();
        when(companyRepository.findAll()).thenReturn(List.of(companyEntity));

        // Act
        List<Company> result = this.companiesService.getCompanies();

        // Assert
        assertEquals(List.of(company), result);
    }
}