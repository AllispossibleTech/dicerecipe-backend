package nl.fontys.dicerecipe.controller;

import nl.fontys.dicerecipe.business.ProductsService;
import nl.fontys.dicerecipe.domain.CompanyDetails;
import nl.fontys.dicerecipe.domain.Product;
import nl.fontys.dicerecipe.domain.requests.CreateProductRequest;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class ProductsControllerUnitTest {

    // for MOCKing
    private final List<Product> serviceResponse = List.of(Product.builder().manufacturer(CompanyDetails.builder().id(1).name("Barilla").address("Via Emilia Romagna 1").vatNumber("IT9302938402").build()).id(1).name("Spaghetti").build());

    // for expected RESPONSE
    private final List<Product> expectedProducts = List.of(Product.builder().manufacturer(CompanyDetails.builder().id(1).name("Barilla").address("Via Emilia Romagna 1").vatNumber("IT9302938402").build()).id(1).name("Spaghetti").build());

    @Test
    void shouldReturn200ResponseWithProductsArray_GetProducts() throws Exception {
        // arrange
        ProductsService productsServiceMock = mock(ProductsService.class);
        when(productsServiceMock.getProducts()).thenReturn(serviceResponse);
        ProductsController productsController = new ProductsController(productsServiceMock);

        //act
        ResponseEntity<List<Product>> actualResponse = productsController.getProducts();

        //assert
        assertEquals(actualResponse.getStatusCode(), HttpStatus.OK, "Response CODE was not correct");
        assertEquals(actualResponse.getBody(), expectedProducts, "Response BODY was not correct");
        verify(productsServiceMock).getProducts();
    }

    @Test
    void shouldReturn201ResponseWithServiceResponseString_CreateProduct() {
        // Arrange
        Product product = Product.builder().manufacturer(CompanyDetails.builder().id(1).name("Barilla").address("Via Emilia Romagna 1").vatNumber("IT9302938402").build()).name("Spaghetti").build();
        ProductsService productsServiceMock = mock(ProductsService.class);
        CreateProductRequest request = CreateProductRequest.builder().product(product).companyId(1).build();
        when(productsServiceMock.createProduct(request)).thenReturn("Product saved with id n.1");
        ProductsController productsController = new ProductsController(productsServiceMock);

        // Act
        ResponseEntity<String> response = productsController.createProduct(request);

        // Assert
        assertEquals(HttpStatus.CREATED, response.getStatusCode());
        assertEquals("Product saved with id n.1", response.getBody());
        verify(productsServiceMock, times(1)).createProduct(request);
    }
}