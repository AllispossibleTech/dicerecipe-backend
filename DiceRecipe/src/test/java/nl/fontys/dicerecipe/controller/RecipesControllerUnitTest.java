package nl.fontys.dicerecipe.controller;

import nl.fontys.dicerecipe.business.RecipesService;
import nl.fontys.dicerecipe.domain.CompanyDetails;
import nl.fontys.dicerecipe.domain.Product;
import nl.fontys.dicerecipe.domain.Recipe;
import nl.fontys.dicerecipe.domain.requests.SearchRecipesByRangeRequest;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;


class RecipesControllerUnitTest {

    // for MOCKing
    private final Product product = Product.builder().manufacturer(CompanyDetails.builder().id(1).name("Barilla").address("Via Emilia Romagna 1").vatNumber("IT9302938402").build()).id(1).name("Spaghetti").unit("grams").build();
    private final List<Recipe> serviceResponse = List.of(new Recipe(1,"Carbonara","Cook Spaghetti", 20, List.of(product), CompanyDetails.builder().id(1).name("Barilla").address("Via Emilia Romagna 1").vatNumber("IT9302938402").build()));

    // for expected RESPONSE
    private final List<Recipe> expectedRecipes = List.of(new Recipe(1,"Carbonara","Cook Spaghetti", 20, List.of(product), CompanyDetails.builder().id(1).name("Barilla").address("Via Emilia Romagna 1").vatNumber("IT9302938402").build()));


    @Test
    void shouldReturn200ResponseWithRecipesArray_getRecipes() throws Exception {
        // arrange
        RecipesService recipesServiceMock = mock(RecipesService.class);
        when(recipesServiceMock.getRecipes()).thenReturn(serviceResponse);
        RecipesController recipesController = new RecipesController(recipesServiceMock);

        //act
        ResponseEntity<List<Recipe>> actualResponse = recipesController.getRecipes();

        //assert
        assertEquals(actualResponse.getStatusCode(), HttpStatus.OK, "Response CODE was not correct");
        assertEquals(actualResponse.getBody(), expectedRecipes, "Response BODY was not correct");
        verify(recipesServiceMock).getRecipes();
    }

    @Test
    void shouldReturn201ResponseWithServiceResponseString_createRecipe() {
        // Arrange
        Recipe recipe = new Recipe(1,"Carbonara","Cook Spaghetti", 20, List.of(product), CompanyDetails.builder().id(1).name("Barilla").address("Via Emilia Romagna 1").vatNumber("IT9302938402").build());
        RecipesService recipesServiceMock = mock(RecipesService.class);
        when(recipesServiceMock.addRecipe(recipe)).thenReturn("Recipe saved with id n.1");
        RecipesController recipesController = new RecipesController(recipesServiceMock);

        // Act
        ResponseEntity<String> response = recipesController.createRecipe(recipe);

        // Assert
        assertEquals(HttpStatus.CREATED, response.getStatusCode());
        assertEquals("Recipe saved with id n.1", response.getBody());
        verify(recipesServiceMock, times(1)).addRecipe(recipe);
    }

    @Test
    void getSuggestedRecipes() {
        // arrange
        RecipesService recipesServiceMock = mock(RecipesService.class);
        when(recipesServiceMock.loadSuggestions(1)).thenReturn(serviceResponse);
        RecipesController recipesController = new RecipesController(recipesServiceMock);

        //act
        ResponseEntity<List<Recipe>> actualResponse = recipesController.getSuggestedRecipes(1);

        //assert
        assertEquals(actualResponse.getStatusCode(), HttpStatus.OK, "Response CODE was not correct");
        assertEquals(actualResponse.getBody(), expectedRecipes, "Response BODY was not correct");
        verify(recipesServiceMock).loadSuggestions(1);
    }

    @Test
    void shouldReturn200ResponseWithRecipesArray_getRecipesByTimeToPrepare() throws Exception {
        // arrange
        RecipesService recipesServiceMock = mock(RecipesService.class);
        SearchRecipesByRangeRequest request = SearchRecipesByRangeRequest.builder().max(5).min(500).build();
        when(recipesServiceMock.getRecipesByRangeOfTimeToPrepare(request)).thenReturn(serviceResponse);
        RecipesController recipesController = new RecipesController(recipesServiceMock);

        //act
        ResponseEntity<List<Recipe>> actualResponse = recipesController.getRecipesByTimeToPrepare(request.getMin(), request.getMax());

        //assert
        assertEquals(actualResponse.getStatusCode(), HttpStatus.OK, "Response CODE was not correct");
        assertEquals(actualResponse.getBody(), expectedRecipes, "Response BODY was not correct");
        verify(recipesServiceMock).getRecipesByRangeOfTimeToPrepare(request);
    }
}