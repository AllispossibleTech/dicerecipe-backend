package nl.fontys.dicerecipe.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import nl.fontys.dicerecipe.domain.CompanyDetails;
import nl.fontys.dicerecipe.domain.NotificationMessage;
import nl.fontys.dicerecipe.domain.Product;
import nl.fontys.dicerecipe.domain.requests.CreateProductRequest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
class NotificationsControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private SimpMessagingTemplate messagingTemplateMock;

    @Test
    void sendNotificationToUsers() throws Exception {
        // arrange
        NotificationMessage message = NotificationMessage.builder().id("1").to("").text("New Recipe Available: Pasta al sugo").from("admin@barilla.com").build();
        ObjectMapper objectMapper = new ObjectMapper();

        // act
        ResultActions resultActions = mockMvc.perform(post("/notifications")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(message)));

        // assert
        resultActions.andDo(print())
                .andExpect(status().isCreated());
        verify(messagingTemplateMock).convertAndSend("/topic/publicmessages", message);
    }
}