package nl.fontys.dicerecipe.controller;

import nl.fontys.dicerecipe.business.LoginService;
import nl.fontys.dicerecipe.domain.login.LoginRequest;
import nl.fontys.dicerecipe.domain.login.LoginResponse;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;
@ExtendWith(MockitoExtension.class)
class LoginControllerUnitTest {

    @Mock
    private LoginService loginService;

    @InjectMocks
    private LoginController loginController;

    @Test
    public void shouldReturn201ResponseWithAccessToken_login() {
        // Arrange
        // Create a sample login request
        LoginRequest loginRequest = new LoginRequest();
        loginRequest.setEmail("testUser");
        loginRequest.setPassword("testPassword");

        // Create a sample login response
        LoginResponse loginResponse = new LoginResponse();
        loginResponse.setAccessToken("sampleToken");


        // Mock the loginService.login() method to return the sample login response
        when(loginService.login(loginRequest)).thenReturn(loginResponse);

        // Act
        ResponseEntity<LoginResponse> responseEntity = loginController.login(loginRequest);

        // Assert
        assertEquals(HttpStatus.CREATED, responseEntity.getStatusCode());
        LoginResponse responseBody = responseEntity.getBody();
        assertEquals(loginResponse.getAccessToken(), responseBody.getAccessToken());

    }
}