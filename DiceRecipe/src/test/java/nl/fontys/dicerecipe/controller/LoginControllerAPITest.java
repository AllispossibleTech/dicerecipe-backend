package nl.fontys.dicerecipe.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import nl.fontys.dicerecipe.business.AccessTokenDecoder;
import nl.fontys.dicerecipe.business.LoginService;
import nl.fontys.dicerecipe.business.ProductsService;
import nl.fontys.dicerecipe.configuration.security.WebSecurityConfig;
import nl.fontys.dicerecipe.domain.CompanyDetails;
import nl.fontys.dicerecipe.domain.Product;
import nl.fontys.dicerecipe.domain.login.LoginRequest;
import nl.fontys.dicerecipe.domain.login.LoginResponse;
import nl.fontys.dicerecipe.domain.requests.CreateProductRequest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
class LoginControllerAPITest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private LoginService loginServiceMock;

    @Test
    void login() throws Exception {
        // arrange
        LoginRequest request = LoginRequest.builder().email("mario@gmail.com").password("mario").build();
        LoginResponse response = LoginResponse.builder().accessToken("fakeAccessToken").build();
        when(loginServiceMock.login(request)).thenReturn(response);
        ObjectMapper objectMapper = new ObjectMapper();

        // act
        ResultActions resultActions = mockMvc.perform(post("/login")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(request)));

        // assert
        resultActions.andDo(print())
                .andExpect(status().isCreated());
        verify(loginServiceMock).login(request);
    }
}