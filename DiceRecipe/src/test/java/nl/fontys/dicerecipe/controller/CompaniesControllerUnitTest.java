package nl.fontys.dicerecipe.controller;

import nl.fontys.dicerecipe.business.CompaniesService;
import nl.fontys.dicerecipe.domain.CompanyDetails;
import nl.fontys.dicerecipe.domain.accounts.Account;
import nl.fontys.dicerecipe.domain.accounts.Company;
import nl.fontys.dicerecipe.domain.accounts.UserType;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class CompaniesControllerUnitTest {

    // for MOCKing
    private final Company company = Company.builder().account(Account.builder().id(1).name("Barilla").email("admin@barilla.it").userType(UserType.COMPANY).build()).id(1).details(CompanyDetails.builder().id(1).name("Barilla").address("Via Emilia Romagna 1").vatNumber("IT9302938402").build()).build();
    private final List<Company> serviceResponse = List.of(company);

    // for expected RESPONSE
    private final List<Company> expectedCompanies = List.of(company);

    @Test
    void shouldReturn200ResponseWithProductsArray_getCompanies() {
        // arrange
        CompaniesService companiesServiceMock = mock(CompaniesService.class);
        when(companiesServiceMock.getCompanies()).thenReturn(serviceResponse);
        CompaniesController companiesController = new CompaniesController(companiesServiceMock);

        //act
        ResponseEntity<List<Company>> actualResponse = companiesController.getCompanies();

        //assert
        assertEquals(actualResponse.getStatusCode(), HttpStatus.OK, "Response CODE was not correct");
        assertEquals(actualResponse.getBody(), expectedCompanies, "Response BODY was not correct");
        verify(companiesServiceMock).getCompanies();
    }

    @Test
    void shouldReturn200ResponseWithCompanyObject_getCompany() {
        // arrange
        CompaniesService companiesServiceMock = mock(CompaniesService.class);
        when(companiesServiceMock.getCompany(1)).thenReturn(Optional.of(company));
        CompaniesController companiesController = new CompaniesController(companiesServiceMock);

        //act
        ResponseEntity<Company> actualResponse = companiesController.getCompany(1);

        //assert
        assertEquals(actualResponse.getStatusCode(), HttpStatus.OK, "Response CODE was not correct");
        assertEquals(actualResponse.getBody(), company, "Response BODY was not correct");
        verify(companiesServiceMock).getCompany(1);
    }

    @Test
    void shouldReturn404NotFoundWhenOptionalIsNull_getCompany() {
        // arrange
        CompaniesService companiesServiceMock = mock(CompaniesService.class);
        when(companiesServiceMock.getCompany(1)).thenReturn(Optional.empty());
        CompaniesController companiesController = new CompaniesController(companiesServiceMock);

        //act
        ResponseEntity<Company> actualResponse = companiesController.getCompany(1);

        //assert
        assertEquals(HttpStatus.NOT_FOUND, actualResponse.getStatusCode(), "Response CODE was not correct");
        assertEquals(null, actualResponse.getBody(), "Response BODY was not correct");
        verify(companiesServiceMock).getCompany(1);
    }

    @Test
    void shouldReturn201ResponseWithServiceResponseString_createCompany() {
        // Arrange
        CompaniesService companiesServiceMock = mock(CompaniesService.class);
        when(companiesServiceMock.createCompany(company)).thenReturn("Company saved with roleId n.1");
        CompaniesController companiesController = new CompaniesController(companiesServiceMock);

        // Act
        ResponseEntity<String> response = companiesController.createCompany(company);

        // Assert
        assertEquals(HttpStatus.CREATED, response.getStatusCode());
        assertEquals("Company saved with roleId n.1", response.getBody());
        verify(companiesServiceMock, times(1)).createCompany(company);
    }
}