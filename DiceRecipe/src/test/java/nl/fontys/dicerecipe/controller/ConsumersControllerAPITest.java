package nl.fontys.dicerecipe.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import nl.fontys.dicerecipe.business.AccessTokenDecoder;
import nl.fontys.dicerecipe.business.ConsumersService;
import nl.fontys.dicerecipe.configuration.security.WebSecurityConfig;
import nl.fontys.dicerecipe.domain.Product;
import nl.fontys.dicerecipe.domain.Recipe;
import nl.fontys.dicerecipe.domain.accounts.Account;
import nl.fontys.dicerecipe.domain.accounts.Consumer;
import nl.fontys.dicerecipe.domain.accounts.UserType;
import nl.fontys.dicerecipe.domain.requests.AddRemoveProductToInventoryRequest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import java.util.ArrayList;
import java.util.Optional;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
class ConsumersControllerAPITest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ConsumersService consumersServiceMock;


    final Account account = Account.builder().id(1).name("Mario").email("mario@gmail.com").password("mario").userType(UserType.CONSUMER).build();
    final Consumer consumer = Consumer.builder().id(1).account(account).doneRecipes(new ArrayList<Recipe>()).foodInventory(new ArrayList<Product>()).preferredCookingTime(30).build();
    private final Optional<Consumer> serviceResponse = Optional.of(consumer);

    private String expectedJSON = "{\"id\":1,\"preferredCookingTime\":30,\"foodInventory\":[],\"doneRecipes\":[],\"account\":{\"id\":1,\"name\":\"Mario\",\"email\":\"mario@gmail.com\",\"password\":\"mario\",\"userType\":\"CONSUMER\"}}";

    @Test
    @WithMockUser(username = "mario@gmail.com", roles = "CONSUMER")
    void shouldReturn200ResponseWithConsumerObject_getConsumer() throws Exception {
        // arrange
        when(consumersServiceMock.getConsumer(1)).thenReturn(serviceResponse);

        //act
        ResultActions resultActions = mockMvc.perform(get("/consumers/1"));

        // assert
        resultActions.andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().json(expectedJSON));
        verify(consumersServiceMock).getConsumer(1);
    }

    @Test
    void createConsumer() throws Exception {
        // arrange
        when(consumersServiceMock.createConsumer(consumer)).thenReturn("Consumer saved with role id n.1");
        ObjectMapper objectMapper = new ObjectMapper();

        // act
        ResultActions resultActions = mockMvc.perform(post("/consumers")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(consumer)));

        // assert
        resultActions.andDo(print())
                .andExpect(status().isCreated());
        verify(consumersServiceMock).createConsumer(consumer);
    }

    @Test
    @WithMockUser(username = "mario@gmail.com", roles = "CONSUMER")
    void addProductToInventory() throws Exception {
        // arrange
        AddRemoveProductToInventoryRequest request = AddRemoveProductToInventoryRequest.builder().productId(1).consumerId(1).build();
        when(consumersServiceMock.addProductToInventory(request)).thenReturn("Updated consumer food inventory now is: 1");
        ObjectMapper objectMapper = new ObjectMapper();

        // act
        ResultActions resultActions = mockMvc.perform(post("/consumers/addproduct")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(request)));

        // assert
        resultActions.andDo(print())
                .andExpect(status().isCreated());
        verify(consumersServiceMock).addProductToInventory(request);
    }

    @Test
    @WithMockUser(username = "mario@gmail.com", roles = "CONSUMER")
    void removeProductToInventory() throws Exception {
        // arrange
        AddRemoveProductToInventoryRequest request = AddRemoveProductToInventoryRequest.builder().productId(1).consumerId(1).build();
        when(consumersServiceMock.removeProductFromInventory(request)).thenReturn("Updated consumer food inventory now is: 0");
        ObjectMapper objectMapper = new ObjectMapper();

        // act
        ResultActions resultActions = mockMvc.perform(post("/consumers/removeproduct")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(request)));

        // assert
        resultActions.andDo(print())
                .andExpect(status().isCreated());
        verify(consumersServiceMock).removeProductFromInventory(request);
    }
}