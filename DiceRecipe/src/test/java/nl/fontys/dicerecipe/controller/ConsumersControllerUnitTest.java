package nl.fontys.dicerecipe.controller;

import nl.fontys.dicerecipe.business.ConsumersService;
import nl.fontys.dicerecipe.domain.Product;
import nl.fontys.dicerecipe.domain.Recipe;
import nl.fontys.dicerecipe.domain.accounts.Account;
import nl.fontys.dicerecipe.domain.accounts.Consumer;
import nl.fontys.dicerecipe.domain.accounts.UserType;
import nl.fontys.dicerecipe.domain.requests.AddRemoveProductToInventoryRequest;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class ConsumersControllerUnitTest {

    // For MOCKing
    final Account account = Account.builder().id(1).name("Mario").email("mario@gmail.com").userType(UserType.CONSUMER).build();
    final Consumer consumer = Consumer.builder().id(1).account(account).doneRecipes(new ArrayList<Recipe>()).foodInventory(new ArrayList<Product>()).preferredCookingTime(30).build();
    private final List<Consumer> serviceResponse = List.of(consumer);

    // for expected RESPONSE
    private final List<Consumer> expectedConsumers = List.of(consumer);

    @Test
    void shouldReturn200ResponseWithCompanyObject_getConsumer() {
        // arrange
        ConsumersService consumersServiceMock = mock(ConsumersService.class);
        when(consumersServiceMock.getConsumer(1)).thenReturn(Optional.of(consumer));
        ConsumersController consumersController = new ConsumersController(consumersServiceMock);

        //act
        ResponseEntity<Consumer> actualResponse = consumersController.getConsumer(1);

        //assert
        assertEquals(actualResponse.getStatusCode(), HttpStatus.OK, "Response CODE was not correct");
        assertEquals(actualResponse.getBody(), consumer, "Response BODY was not correct");
        verify(consumersServiceMock).getConsumer(1);
    }

    @Test
    void shouldReturn404NotFound_WhenOptionalIsNull_getConsumer() {
        // arrange
        ConsumersService consumersServiceMock = mock(ConsumersService.class);
        when(consumersServiceMock.getConsumer(1)).thenReturn(Optional.empty());
        ConsumersController consumersController = new ConsumersController(consumersServiceMock);

        //act
        ResponseEntity<Consumer> actualResponse = consumersController.getConsumer(1);

        //assert
        assertEquals(HttpStatus.NOT_FOUND, actualResponse.getStatusCode(), "Response CODE was not correct");
        assertEquals(null, actualResponse.getBody(), "Response BODY was not correct");
        verify(consumersServiceMock).getConsumer(1);
    }

    @Test
    void shouldReturn201ResponseWithServiceResponseString_createConsumer() {
        // Arrange
        ConsumersService consumersServiceMock = mock(ConsumersService.class);
        when(consumersServiceMock.createConsumer(consumer)).thenReturn("Consumer saved with roleId n.1");
        ConsumersController consumersController = new ConsumersController(consumersServiceMock);

        // Act
        ResponseEntity<String> response = consumersController.createConsumer(consumer);

        // Assert
        assertEquals(HttpStatus.CREATED, response.getStatusCode());
        assertEquals("Consumer saved with roleId n.1", response.getBody());
        verify(consumersServiceMock, times(1)).createConsumer(consumer);
    }

    @Test
    void shouldReturn201ResponseWithServiceResponseString_addProductToInventory() {
        // Arrange
        ConsumersService consumersServiceMock = mock(ConsumersService.class);
        AddRemoveProductToInventoryRequest request = AddRemoveProductToInventoryRequest.builder().productId(1).consumerId(1).build();
        when(consumersServiceMock.addProductToInventory(request)).thenReturn("Updated consumer food inventory now is: 1");
        ConsumersController consumersController = new ConsumersController(consumersServiceMock);

        // Act
        ResponseEntity<String> response = consumersController.addProductToInventory(request);

        // Assert
        assertEquals(HttpStatus.CREATED, response.getStatusCode());
        assertEquals("Updated consumer food inventory now is: 1", response.getBody());
        verify(consumersServiceMock, times(1)).addProductToInventory(request);
    }

    @Test
    void shouldReturn201ResponseWithServiceResponseString_removeProductToInventory() {
        // Arrange
        ConsumersService consumersServiceMock = mock(ConsumersService.class);
        AddRemoveProductToInventoryRequest request = AddRemoveProductToInventoryRequest.builder().productId(1).consumerId(1).build();
        when(consumersServiceMock.removeProductFromInventory(request)).thenReturn("Updated consumer food inventory now is: 0");
        ConsumersController consumersController = new ConsumersController(consumersServiceMock);

        // Act
        ResponseEntity<String> response = consumersController.removeProductToInventory(request);

        // Assert
        assertEquals(HttpStatus.CREATED, response.getStatusCode());
        assertEquals("Updated consumer food inventory now is: 0", response.getBody());
        verify(consumersServiceMock, times(1)).removeProductFromInventory(request);
    }
}