package nl.fontys.dicerecipe.controller;

import nl.fontys.dicerecipe.business.RecipesService;
import nl.fontys.dicerecipe.domain.CompanyDetails;
import nl.fontys.dicerecipe.domain.Product;
import nl.fontys.dicerecipe.domain.Recipe;
import nl.fontys.dicerecipe.domain.requests.SearchRecipesByRangeRequest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.List;
import java.util.Optional;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
class RecipesControllerAPITest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private RecipesService recipesServiceMock;


    private final Product product = Product.builder().manufacturer(CompanyDetails.builder().id(1).name("Barilla").address("Via Emilia Romagna 1").vatNumber("IT9302938402").build()).id(1).name("Spaghetti").unit("grams").build();
    private final List<Recipe> serviceResponse = List.of(new Recipe(1,"Carbonara","Cook Spaghetti", 20, List.of(product), CompanyDetails.builder().id(1).name("Barilla").address("Via Emilia Romagna 1").vatNumber("IT9302938402").build()));

    private String expectedJSON = "[{\"id\":1,\"title\":\"Carbonara\",\"description\":\"Cook Spaghetti\",\"timeToPrepare\":20,\"ingredients\":[{\"id\":1,\"name\":\"Spaghetti\",\"unit\":\"grams\",\"manufacturer\":{\"id\":1,\"address\":\"Via Emilia Romagna 1\",\"name\":\"Barilla\",\"vatNumber\":\"IT9302938402\"}}],\"companyDetails\":{\"id\":1,\"address\":\"Via Emilia Romagna 1\",\"name\":\"Barilla\",\"vatNumber\":\"IT9302938402\"}}]";

    @Test
    void getRecipes_shouldReturn200ResponseWithRecipesArray() throws Exception {
        // arrange
        when(recipesServiceMock.getRecipes()).thenReturn(serviceResponse);

        //act
        ResultActions resultActions = mockMvc.perform(get("/recipes"));

        // assert
        resultActions.andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().json(expectedJSON));
        verify(recipesServiceMock).getRecipes();
    }

    @Test
    @WithMockUser(username = "admin@barilla.com", roles = "COMPANY")
    void createRecipe_shouldReturn201Response() throws Exception {
        // arrange
        Recipe recipe = new Recipe(2, "Lasagna", "Bake lasagna", 30, List.of(product), CompanyDetails.builder().id(1).name("Barilla").address("Via Emilia Romagna 1").vatNumber("IT9302938402").build());

        ObjectMapper objectMapper = new ObjectMapper();

        // act
        ResultActions resultActions = mockMvc.perform(post("/recipes")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(recipe)));

        // assert
        resultActions.andDo(print())
                .andExpect(status().isCreated());
        verify(recipesServiceMock).addRecipe(recipe);
    }

    @Test
    @WithMockUser(username = "mario@gmail.com", roles = "CONSUMER")
    void getSuggestedRecipes_shouldReturn200Response() throws Exception {
        // arrange
        int consumerId = 1;
        when(recipesServiceMock.loadSuggestions(consumerId)).thenReturn(serviceResponse);

        // act
        ResultActions resultActions = mockMvc.perform(get("/recipes/loadsuggestions/{id}", consumerId));

        // assert
        resultActions.andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().json(expectedJSON));
        verify(recipesServiceMock).loadSuggestions(consumerId);
    }

    @Test
    @WithMockUser(username = "admin@barilla.com", roles = "COMPANY")
    void getRecipesByCompanyId_shouldReturn200ResponseWithRecipesArray() throws Exception {
        // arrange
        int companyId = 1;
        when(recipesServiceMock.getRecipesByCompanyDetailsId(companyId)).thenReturn(serviceResponse);

        // act
        ResultActions resultActions = mockMvc.perform(get("/recipes/company/{id}", companyId));

        // assert
        resultActions.andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().json(expectedJSON));
        verify(recipesServiceMock).getRecipesByCompanyDetailsId(companyId);
    }

    @Test
    @WithMockUser(username = "admin@barilla.com", roles = "COMPANY")
    void deleteProduct_shouldReturn200Response() throws Exception {
        // arrange
        Recipe recipe = new Recipe(1, "Carbonara", "Cook Spaghetti", 20, List.of(product), CompanyDetails.builder().id(1).name("Barilla").address("Via Emilia Romagna 1").vatNumber("IT9302938402").build());

        ObjectMapper objectMapper = new ObjectMapper();

        // act
        ResultActions resultActions = mockMvc.perform(post("/recipes/delete")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(recipe)));

        // assert
        resultActions.andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string("deleted"));
        verify(recipesServiceMock).delete(recipe);
    }

    @Test
    @WithMockUser(username = "admin@barilla.com", roles = "CONSUMER")
    void getRecipesByTimeToPrepare_shouldReturn200ResponseWithRecipesArray() throws Exception {
        // arrange
        int timeToPrepare = 22;
        when(recipesServiceMock.getRecipesByTimeToPrepare(timeToPrepare)).thenReturn(serviceResponse);

        // act
        ResultActions resultActions = mockMvc.perform(get("/recipes/time-to-prepare/{id}", timeToPrepare));

        // assert
        resultActions.andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().json(expectedJSON));
        verify(recipesServiceMock).getRecipesByTimeToPrepare(timeToPrepare);
    }

    @Test
    @WithMockUser(username = "admin@barilla.com", roles = "COMPANY")
    void getRecipeById_shouldReturn200ResponseWithRecipeObject() throws Exception {
        // arrange
        int recipeId = 1;
        String expectedJSON_Object = "{\"id\":1,\"title\":\"Carbonara\",\"description\":\"Cook Spaghetti\",\"timeToPrepare\":20,\"ingredients\":[{\"id\":1,\"name\":\"Spaghetti\",\"unit\":\"grams\",\"manufacturer\":{\"id\":1,\"address\":\"Via Emilia Romagna 1\",\"name\":\"Barilla\",\"vatNumber\":\"IT9302938402\"}}],\"companyDetails\":{\"id\":1,\"address\":\"Via Emilia Romagna 1\",\"name\":\"Barilla\",\"vatNumber\":\"IT9302938402\"}}";
        Recipe recipe = new Recipe(1,"Carbonara","Cook Spaghetti", 20, List.of(product), CompanyDetails.builder().id(1).name("Barilla").address("Via Emilia Romagna 1").vatNumber("IT9302938402").build());
        when(recipesServiceMock.getRecipeById(recipeId)).thenReturn(recipe);

        // act
        ResultActions resultActions = mockMvc.perform(get("/recipes/{id}", recipeId));

        // assert
        resultActions.andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().json(expectedJSON_Object));
        verify(recipesServiceMock).getRecipeById(recipeId);
    }

    @Test
    void getRecipesByRangeOfTimeToPrepare_shouldReturn200ResponseWithRecipesArray() throws Exception {
        // arrange
        SearchRecipesByRangeRequest request = SearchRecipesByRangeRequest.builder().max(5).min(500).build();
        when(recipesServiceMock.getRecipesByRangeOfTimeToPrepare(request)).thenReturn(serviceResponse);
        ObjectMapper objectMapper = new ObjectMapper();

        // act
        ResultActions resultActions = mockMvc.perform(get("/recipes/time-to-prepare-range")
                .param("min", String.valueOf(request.getMin()))
                .param("max", String.valueOf(request.getMax()))
                .contentType(MediaType.APPLICATION_JSON));

        // assert
        resultActions.andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().json(expectedJSON));
        verify(recipesServiceMock).getRecipesByRangeOfTimeToPrepare(request);
    }
}