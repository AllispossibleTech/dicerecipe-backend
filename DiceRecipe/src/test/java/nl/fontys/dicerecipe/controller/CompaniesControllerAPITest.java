package nl.fontys.dicerecipe.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import nl.fontys.dicerecipe.business.CompaniesService;
import nl.fontys.dicerecipe.domain.CompanyDetails;
import nl.fontys.dicerecipe.domain.accounts.Account;
import nl.fontys.dicerecipe.domain.accounts.Company;
import nl.fontys.dicerecipe.domain.accounts.UserType;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import java.util.List;
import java.util.Optional;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
class CompaniesControllerAPITest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private CompaniesService companiesServiceMock;

    private final Company company = Company.builder().account(Account.builder().id(1).name("Barilla").email("admin@barilla.it").password("barilla").userType(UserType.COMPANY).build()).id(1).details(CompanyDetails.builder().id(1).name("Barilla").address("Via Mantova 166 Parma").vatNumber("01654010345").build()).build();
    private final Optional<Company> serviceResponse = Optional.of(company);
    private final List<Company> serviceResponseList = List.of(company);

    private String expectedJSON = "{\"id\":1,\"details\":{\"id\":1,\"address\":\"Via Mantova 166 Parma\",\"name\":\"Barilla\",\"vatNumber\":\"01654010345\"},\"account\":{\"id\":1,\"name\":\"Barilla\",\"email\":\"admin@barilla.it\",\"password\":\"barilla\",\"userType\":\"COMPANY\"}}";

    @Test
    void getCompanies() throws Exception {
        // arrange
        final String expectedJSONList = "[{\"id\":1,\"details\":{\"id\":1,\"address\":\"Via Mantova 166 Parma\",\"name\":\"Barilla\",\"vatNumber\":\"01654010345\"},\"account\":{\"id\":1,\"name\":\"Barilla\",\"email\":\"admin@barilla.it\",\"password\":\"barilla\",\"userType\":\"COMPANY\"}}]";
        when(companiesServiceMock.getCompanies()).thenReturn(serviceResponseList);

        //act
        ResultActions resultActions = mockMvc.perform(get("/companies"));

        // assert
        resultActions.andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().json(expectedJSONList));
        verify(companiesServiceMock).getCompanies();
    }

    @Test
    void getCompany() throws Exception {
        // arrange
        when(companiesServiceMock.getCompany(1)).thenReturn(serviceResponse);

        //act
        ResultActions resultActions = mockMvc.perform(get("/companies/1"));

        // assert
        resultActions.andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().json(expectedJSON));
        verify(companiesServiceMock).getCompany(1);
    }

    @Test
    void createCompany() throws Exception {
        // arrange
        when(companiesServiceMock.createCompany(company)).thenReturn("Company saved with role id n.1");
        ObjectMapper objectMapper = new ObjectMapper();

        // act
        ResultActions resultActions = mockMvc.perform(post("/companies")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(company)));

        // assert
        resultActions.andDo(print())
                .andExpect(status().isCreated());
        verify(companiesServiceMock).createCompany(company);
    }
}