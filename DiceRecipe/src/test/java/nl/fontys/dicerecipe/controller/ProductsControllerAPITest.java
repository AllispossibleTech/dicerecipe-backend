package nl.fontys.dicerecipe.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import nl.fontys.dicerecipe.business.AccessTokenDecoder;
import nl.fontys.dicerecipe.business.ProductsService;
import nl.fontys.dicerecipe.configuration.security.WebSecurityConfig;
import nl.fontys.dicerecipe.domain.CompanyDetails;
import nl.fontys.dicerecipe.domain.Product;
import nl.fontys.dicerecipe.domain.Recipe;
import nl.fontys.dicerecipe.domain.requests.CreateProductRequest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import java.util.List;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
class ProductsControllerAPITest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ProductsService productsServiceMock;

    private final List<Product> serviceResponse = List.of(Product.builder().manufacturer(CompanyDetails.builder().id(1).name("Barilla").address("Via Emilia Romagna 1").vatNumber("IT9302938402").build()).id(1).name("Spaghetti").unit("grams").build());

    private String expectedJSON = "[{\"id\":1,\"name\":\"Spaghetti\",\"unit\":\"grams\",\"manufacturer\":{\"id\":1,\"address\":\"Via Emilia Romagna 1\",\"name\":\"Barilla\",\"vatNumber\":\"IT9302938402\"}}]";

    @Test
    void getProducts_shouldReturn200ResponseWithProductsArray() throws Exception {
        // arrange
        when(productsServiceMock.getProducts()).thenReturn(serviceResponse);

        //act
        ResultActions resultActions = mockMvc.perform(get("/products"));

        // assert
        resultActions.andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().json(expectedJSON));
        verify(productsServiceMock).getProducts();
    }

    @Test
    @WithMockUser(username = "admin@barilla.com", roles = "COMPANY")
    void createProduct_shouldReturn201Response() throws Exception {
        // arrange
        Product product = Product.builder().manufacturer(CompanyDetails.builder().id(1).name("Barilla").address("Via Emilia Romagna 1").vatNumber("IT9302938402").build()).id(1).name("Spaghetti").unit("grams").build();
        CreateProductRequest request = CreateProductRequest.builder().companyId(1).product(product).build();
        ObjectMapper objectMapper = new ObjectMapper();

        // act
        ResultActions resultActions = mockMvc.perform(post("/products")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(request)));

        // assert
        resultActions.andDo(print())
                .andExpect(status().isCreated());
        verify(productsServiceMock).createProduct(request);
    }

    @Test
    void searchProduct_shouldReturn200ResponseWithProductsArray() throws Exception {
        // arrange
        String query = "Spaghetti";
        when(productsServiceMock.search(query)).thenReturn(serviceResponse);

        // act
        ResultActions resultActions = mockMvc.perform(get("/products/search/"+query)
                .contentType(MediaType.APPLICATION_JSON)
                .content(query));

        // assert
        resultActions.andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().json(expectedJSON));
        verify(productsServiceMock).search(query);
    }

    @Test
    @WithMockUser(username = "admin@barilla.com", roles = "COMPANY")
    void getProductsByCompanyId_shouldReturn200ResponseWithProductsArray() throws Exception {
        // arrange
        int companyId = 1;
        when(productsServiceMock.getProductsByCompanyDetailsId(companyId)).thenReturn(serviceResponse);

        // act
        ResultActions resultActions = mockMvc.perform(get("/products/{id}", companyId));

        // assert
        resultActions.andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().json(expectedJSON));
        verify(productsServiceMock).getProductsByCompanyDetailsId(companyId);
    }

    @Test
    @WithMockUser(username = "admin@barilla.com", roles = "COMPANY")
    void deleteProduct_shouldReturn200Response() throws Exception {
        // arrange
        Product product = Product.builder().manufacturer(CompanyDetails.builder().id(1).name("Barilla").address("Via Emilia Romagna 1").vatNumber("IT9302938402").build()).id(1).name("Spaghetti").unit("grams").build();
        ObjectMapper objectMapper = new ObjectMapper();

        // act
        ResultActions resultActions = mockMvc.perform(post("/products/delete")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(product)));

        // assert
        resultActions.andDo(print())
                .andExpect(status().isOk());
        verify(productsServiceMock).delete(product);
    }

}