package nl.fontys.dicerecipe.controller;

import lombok.AllArgsConstructor;
import nl.fontys.dicerecipe.business.ProductsService;
import nl.fontys.dicerecipe.configuration.security.isauthenticated.IsAuthenticated;
import nl.fontys.dicerecipe.domain.Product;
import nl.fontys.dicerecipe.domain.requests.CreateProductRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.security.RolesAllowed;
import java.util.List;

@RestController
@RequestMapping("/products")
@AllArgsConstructor
@CrossOrigin(origins="http://localhost:3000")
public class ProductsController {
    private final ProductsService productsService;

    @GetMapping
    public ResponseEntity<List<Product>> getProducts() {
        return ResponseEntity.ok(productsService.getProducts());
    }

    @IsAuthenticated
    @RolesAllowed({"COMPANY"})
    @PostMapping()
    public ResponseEntity<String> createProduct(@RequestBody CreateProductRequest request) {
        String response = productsService.createProduct(request);
        return ResponseEntity.status(HttpStatus.CREATED).body(response);
    }

    @GetMapping("/search/{query}")
    public ResponseEntity<List<Product>> searchProduct(@PathVariable(value = "query") final String query) {
        List<Product> response = productsService.search(query);
        return ResponseEntity.status(HttpStatus.OK).body(response);
    }

    @IsAuthenticated
    @RolesAllowed({"COMPANY"})
    @GetMapping("{id}")
    public ResponseEntity<List<Product>> getProductsByCompanyId(@PathVariable(value = "id") final int id) {
        return ResponseEntity.ok(productsService.getProductsByCompanyDetailsId(id));
    }

    @IsAuthenticated
    @RolesAllowed({"COMPANY"})
    @PostMapping("/delete")
    public ResponseEntity<String> deleteProduct(@RequestBody Product product) {
        String response = productsService.delete(product);
        return ResponseEntity.ok(response);
    }

}