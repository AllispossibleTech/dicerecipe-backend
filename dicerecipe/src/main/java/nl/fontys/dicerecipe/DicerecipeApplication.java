package nl.fontys.dicerecipe;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DicerecipeApplication {

    public static void main(String[] args) {
        SpringApplication.run(DicerecipeApplication.class, args);
    }

}
