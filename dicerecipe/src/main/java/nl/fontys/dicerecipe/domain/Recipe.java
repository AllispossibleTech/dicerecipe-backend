package nl.fontys.dicerecipe.domain;

import lombok.*;

import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class Recipe {
    private int id;
    private String title;
    private String description;
    private int timeToPrepare;
    private List<Product> ingredients;
    private CompanyDetails companyDetails;
}
