package nl.fontys.dicerecipe.domain;

import lombok.*;

@EqualsAndHashCode
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data

public class Product {
    private int id;
    private String name;
    private String unit;
    private CompanyDetails manufacturer;

    public Product(int id, String name, String unit) {
        this.id = id;
        this.name = name;
        this.unit = unit;
    }
}