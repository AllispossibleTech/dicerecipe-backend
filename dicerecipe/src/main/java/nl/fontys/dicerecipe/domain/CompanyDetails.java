package nl.fontys.dicerecipe.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Builder
@Data
@AllArgsConstructor
public class CompanyDetails {
    private int id;
    private String address;
    private String name;
    private String vatNumber;
}