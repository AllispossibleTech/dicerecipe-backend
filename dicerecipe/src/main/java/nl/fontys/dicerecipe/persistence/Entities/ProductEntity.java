package nl.fontys.dicerecipe.persistence.Entities;

import javax.persistence.*;

import lombok.*;

import java.util.Set;

@Entity
@Table(name = "products")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ProductEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    //@NotBlank
    //@Length(min = 2, max = 50)
    @Column(name = "name")
    private String name;

    //@NotBlank
    //@Length(min = 2, max = 50)
    @Column(name = "unit")
    private String unit;

    @ManyToOne
    @JoinColumn(name = "companydetails_id")
    private CompanyDetailsEntity companyDetailsEntity;

    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @ManyToMany(mappedBy = "ingredients")
    Set<RecipeEntity> usages;

    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @ManyToMany(mappedBy = "foodInventoryEntities")
    Set<ConsumerEntity> ownedBy;
}
