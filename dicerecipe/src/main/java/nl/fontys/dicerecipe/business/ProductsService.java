package nl.fontys.dicerecipe.business;

import nl.fontys.dicerecipe.domain.Product;
import nl.fontys.dicerecipe.domain.requests.CreateProductRequest;

import java.sql.SQLIntegrityConstraintViolationException;
import java.util.List;

public interface ProductsService {
    List<Product> getProducts();
    List<Product> getProductsByCompanyDetailsId(int id);
    List<Product> search(String query);
    String createProduct(CreateProductRequest request);
    String delete(Product product);
}
