package nl.fontys.dicerecipe.business.impl;

import lombok.AllArgsConstructor;
import nl.fontys.dicerecipe.business.CompaniesService;
import nl.fontys.dicerecipe.business.ProductsService;
import nl.fontys.dicerecipe.business.impl.converters.CompanyDetailsConverter;
import nl.fontys.dicerecipe.business.impl.converters.ProductConverter;
import nl.fontys.dicerecipe.domain.Product;
import nl.fontys.dicerecipe.domain.accounts.Company;
import nl.fontys.dicerecipe.domain.requests.CreateProductRequest;
import nl.fontys.dicerecipe.persistence.DataAccessImpl.SpringImpl.ProductJPA;
import nl.fontys.dicerecipe.persistence.Entities.CompanyDetailsEntity;
import nl.fontys.dicerecipe.persistence.Entities.ProductEntity;
import org.springframework.stereotype.Service;

import java.sql.SQLIntegrityConstraintViolationException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@AllArgsConstructor
@Service
public class ProductsServiceImpl implements ProductsService {

    private final ProductJPA productsRepository;
    private final CompaniesService companiesService;

    @Override
    public List<Product> getProducts() {
        List<Product> products = productsRepository.findAll()
                .stream()
                .map(ProductConverter::convert)
                .toList();
        return products;
    }

    @Override
    public List<Product> getProductsByCompanyDetailsId(int id) {
        List<Product> result = new ArrayList<>();
        for (Product p : this.getProducts())
        {
            if (p.getManufacturer().getId() == id)
            {
                result.add(p);
            }
        }
        return result;
    }

    @Override
    public List<Product> search(String query) {
        List<Product> result = new ArrayList<>();
        for (Product p : this.getProducts()) {
            if (p.getName().toLowerCase().contains(query.toLowerCase())) {
                result.add(p);
            }
        }
        return result;
    }

    @Override
    public String createProduct(CreateProductRequest request) {
        Optional<Company> optionalManufacturer = companiesService.getCompany(request.getCompanyId());

        if (optionalManufacturer.isPresent()) {
            Company manufacturer = optionalManufacturer.get();
            CompanyDetailsEntity companyDetailsToSave = CompanyDetailsConverter.convertToEntity(manufacturer.getDetails());

            ProductEntity toSave = ProductEntity.builder()
                    .id(request.getProduct().getId())
                    .name(request.getProduct().getName())
                    .unit(request.getProduct().getUnit())
                    .companyDetailsEntity(companyDetailsToSave)
                    .build();
            ProductEntity saved = productsRepository.save(toSave);
            return "Product saved with id n."+saved.getId();
        }
        return "Error: Company with that Id not found!";
    }

    @Override
    public String delete(Product product) {
        if (productsRepository.existsById(product.getId()))
        {
            try {
                productsRepository.delete(ProductConverter.convertToEntity(product));
            } catch (Exception exception) {
                return "Product cannot be deleted because it's part of a Recipe as an ingredient!";
            }
            return "Product exists and is going to be deleted!";
        }
        return "Product does not exist";
    }
}